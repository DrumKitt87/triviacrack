class GameChallenge < ActiveRecord::Base
  has_many :questions, -> { uniq }, :through => :game_challenge_questions
end
