class GameDetail < ActiveRecord::Base
  belongs_to :game
  has_one :user
  has_many :game_pieces, -> { uniq }, :through => :game_detail_game_pieces
  has_many :questions, -> { uniq }, :through => :game_detail_questions
  has_many :answered_questions
  has_many :game_challenges
end
