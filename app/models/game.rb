class Game < ActiveRecord::Base
  has_many :game_details
  has_many :users
end
