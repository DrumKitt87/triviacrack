class GameChallengeQuestion < ActiveRecord::Base
  belongs_to :game_challenge
  belongs_to :question
end
