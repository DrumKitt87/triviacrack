class User < ActiveRecord::Base
  belongs_to :game
  belongs_to :game_detail
  def self.create_from_omniauth(params)
    if(params['info'].nil? == false && params['info']['image'].nil? == false)
      image = params['info']['image']
    else
      image = asset_path + 'images/default-player-icon.png'
    end

    attributes = {
      email: params['info']['email'],
      provider: params['provider'],
      uid: params['uid'],
      password: Devise.friendly_token,
      image: image
    }

    create(attributes)
  end

  has_many :authentications, class_name: 'UserAuthentication', dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable

end
