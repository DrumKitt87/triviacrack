class Question < ActiveRecord::Base
  belongs_to :category
  has_many :answers
  has_many :game_details, :through => :game_detail_questions
end
