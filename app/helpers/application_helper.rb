module ApplicationHelper

  def check_for_user
    if(current_user.nil? == true)
      redirect_to welcome_index_path
    end
  end

  def get_opponent game
    if game.player1_id == current_user.id
      User.where(id: game.player2_id).first
    else
      User.where(id: game.player1_id).first
    end
  end

  def handle_player_leveling
    level_calc = ((current_user.xp.to_f / GlobalConstants::LEVEL_UP_INTERVAL.to_f).to_i + 1)
    if(level_calc > current_user.level)
      current_user.level = level_calc
    end
  end

end
