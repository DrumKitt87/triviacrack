# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

addBorderCrown = (element) ->
  $("[data-add-border]").parent().removeClass('category-border')
  $(".selected-category-checkmark").addClass('hidden')
  $("[data-add-border]").removeAttr('selected')
  $(element).parent().addClass('category-border')
  $(element).find('span').removeClass('hidden')
  $(element).attr('selected', 'selected')
  $('#choose-category-submit').removeClass('disabled')

$ ->
  $("div[data-add-border]").click (e) ->
    addBorderCrown(this)


addBorderChallengeOpponent = (element) ->
  $("[data-add-border-challenge-opponent]").parent().removeClass('category-border')
  $(".opponent-character.selected-category-checkmark-challenge").addClass('hidden')
  $("[data-add-border-challenge-opponent]").removeAttr('selected')

  $(element).parent().addClass('category-border')
  $(element).find('span').removeClass('hidden')
  $(element).attr('selected', 'selected')
  $('#choose-category-submit').removeClass('disabled') if $('[selected]').size() == 2

$ ->
  $("div[data-add-border-challenge-opponent]").click (e) ->
    addBorderChallengeOpponent(this)


addBorderChallengeWager = (element) ->
  $("[data-add-border-challenge-wager]").parent().removeClass('category-border')
  $(".wager-character.selected-category-checkmark-challenge").addClass('hidden')
  $("[data-add-border-challenge-wager]").removeAttr('selected')

  $(element).parent().addClass('category-border')
  $(element).find('span').removeClass('hidden')
  $(element).attr('selected', 'selected')
  $('#choose-category-submit').removeClass('disabled') if $('[selected]').size() == 2

$ ->
  $("div[data-add-border-challenge-wager]").click (e) ->
    addBorderChallengeWager(this)

#$ ->
#  $('#spinner_button').click ->
#      $('#spinner_image').rotate({
#      animateTo: 1455, easing: (x, t, b, c, d) ->
#        return (c * (t / (d * 2)) + b)
#  })