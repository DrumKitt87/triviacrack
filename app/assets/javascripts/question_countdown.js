var runCountdown = function(){
    var countdownBar = document.getElementById('countdown-bar');
    var time = 0.0;
    var currentIntervalId = 0;
    question_countdown_interval = currentIntervalId = setInterval(function(){
        if(currentIntervalId === question_countdown_interval) {
            if (time + 0.1 > 60.0) {
                clearInterval(question_countdown_interval);
                $('#unanswered_question_remote_call')[0].click();
            } else {
                time += 0.1;
                $('#countdown_header').text('' + Math.round(60.0 - time));
                if (time > 29.5 && time < 49.5) {
                    $('#countdown_header').css('color', 'orange');
                    $(countdownBar).addClass('progress-bar-warning');
                }
                else if (time >= 49.5) {
                    $('#countdown_header').css('color', 'red');
                    $(countdownBar).removeClass('progress-bar-warning');
                    $(countdownBar).addClass('progress-bar-danger');
                }
                $(countdownBar).css('width', '' + ((time / 60.0) * 100.0) + '%')
            }
        }
    }, 100);
}

runCountdown();