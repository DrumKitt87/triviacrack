# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# app/assets/javascripts/something.js.coffee
$ ->
  friends_checkbox = $("#friends_checkbox")
  random_checkbox = $('#random_checkbox')

  # Show the button as clicked if the radio button is checked

  if friends_checkbox.is ':checked'
    friends_checkbox.parent().addClass("active")
  else if random_checkbox.is ':checked'
    random_checkbox.parent().addClass("active")