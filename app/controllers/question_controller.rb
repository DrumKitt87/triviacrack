  class QuestionController < ApplicationController

  respond_to :html, :js

  include ApplicationHelper

  def question
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      game_piece = GamePiece.where(category_id: params[:category_id]).first;
      @game_piece_image_url = game_piece.nil? == false ? game_piece.image_url : '';

      @is_crown = params[:is_crown]
      @is_challenge = params[:is_challenge]
      if(@is_challenge == :true.to_s)
        @challenge = GameChallenge.where(id: params[:challenge_id]).first
      end
      @game = Game.where(id: params[:game_id]).first
      @game_detail = @game.game_details.where(player_id: current_user.id).first
      if(@game_detail.correct_answers > 3 || @is_crown == 'true')
        @game_detail.shown_crown_message= true
        @game_detail.save
      end
      @category = Category.where(id: params[:category_id]).first
      @questions_asked_ids = GameDetailQuestion.where(game_detail_id: @game_detail.id).uniq.pluck(:question_id)

      # TODO: remove && false once more questions are seeded into db
      if(@questions_asked_ids.count > 0)
        @questions = Question.where('category_id = ? AND active = ? AND id NOT IN (?)', @category.id, true, @questions_asked_ids)
      else
        @questions = Question.where('category_id = ? AND active = ?', @category.id, true)
      end
      if(@questions.count < 1)
        flash[:error] = 'Oh no! I guess we need to add more questions to the pool.'
        redirect_to home_index_path
      else
        if(@is_crown == 'true')
          @game_detail.correct_answers = 0;
          @game_detail.save
        end
        @question = @questions.offset(rand(@questions.count)).first
        @answers = @question.answers.shuffle
        @correct_answer = @question.answers.where(is_correct: true).first
        GameDetailQuestion.create(game_detail_id: @game_detail.id, question_id: @question.id, question_answered: false)
      end
    else
      redirect_to welcome_index_path
    end

  end

  def answered_question
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @is_crown = params[:is_crown]
      @is_challenge = params[:is_challenge]
      @did_not_answer = params[:did_not_answer] == 'true' || false
      @power_up_skip = params[:power_up_skip] == 'true' || false

      puts 'did_not_answer: '
      puts @did_not_answer

      if(@is_challenge == :true.to_s)
        @challenge = GameChallenge.where(id: params[:challenge_id]).first
      else
        @challenge = nil
      end

      @game = Game.where(id: params[:game_id]).first
      @game_detail = @game.game_details.where(player_id: current_user.id).first
      @chosen_answer = Answer.where(id: params[:answer_id]).first

      @question = Question.where(id: params[:question_id]).first
      @answered_correctly = @did_not_answer == true ? false : @chosen_answer.is_correct
      @category = Category.where(id: @question.category_id).first

      @game_detail_question = GameDetailQuestion.where('game_detail_id= ? AND question_id= ?', @game_detail.id, @question.id).first

      # TODO: remove true once more questions are entered
      if(@game_detail_question.question_answered == false)
        @game_detail_question.question_answered= true
        @game_detail_question.save
        if(@power_up_skip == false)
          update_game_details(@game, @game_detail, @chosen_answer, @category, @is_crown, @is_challenge, @challenge, @did_not_answer)
        end
      else
        @player_tried_to_re_answer = true
      end
    else
      redirect_to welcome_index_path
    end
  end

  private

  def update_game_details(game, game_detail, answer, category, is_crown, is_challenge, challenge, did_not_answer)
    @answered_question = AnsweredQuestion.where('game_detail_id= ? AND category_id= ?', game_detail.id, category.id).first
    if(did_not_answer == false && answer.is_correct == true)
      if(is_crown == :true.to_s)
        handle_correct_crown(game, game_detail, category)
      elsif(is_challenge == :true.to_s)
        handle_correct_challenge(game_detail, challenge)
      else
        handle_correct_answer(game_detail)
      end
      game_detail.save
      if(@answered_question.nil? == true)
        AnsweredQuestion.create(game_detail_id: game_detail.id, category_id: category.id, correct_count: 1)
      else
        @answered_question.correct_count= @answered_question.correct_count + 1
        @answered_question.save
      end
    else
      if(@answered_question.nil? == true)
        AnsweredQuestion.create(game_detail_id: game_detail.id, category_id: category.id, incorrect_count: 1)
      else
        @answered_question.incorrect_count= @answered_question.incorrect_count + 1
        @answered_question.save
      end
      if(is_challenge == :true.to_s)
        handle_incorrect_challenge(game_detail, challenge)
      else
        handle_incorrect_answer(game, game_detail)
      end
    end
  end

  private

  def handle_correct_crown(game, game_detail, category)
    game_detail.correct_answers= 0
    game_detail.shown_crown_message = false
    @game_piece = GamePiece.where(category_id: category.id).first
    @player_game_pieces = GameDetailGamePiece.where(game_detail_id: @game_detail.id).pluck(:game_piece_id)
    if(@game_piece.nil? == false && @player_game_pieces.include?(@game_piece.id) == false)
      GameDetailGamePiece.create(game_detail_id: game_detail.id, game_piece_id: @game_piece.id)
    end
    total_game_pieces = GamePiece.all
    if(GameDetailGamePiece.where(game_detail_id: @game_detail.id).count == total_game_pieces.count)
      game.finished= true
      game.save
    end
    current_user.xp += GlobalConstants::CROWN_XP_VALUE
    handle_player_leveling
    current_user.currency += GlobalConstants::CROWN_CURRENCY_VALUE
    current_user.save
  end

  def handle_correct_challenge(game_detail, challenge)
    game_detail.correct_answers = 0
    game_detail.save
    if(current_user.id == challenge.player_id)
      challenge.player_correct_count = challenge.player_correct_count + 1
    elsif(current_user.id == challenge.opponent_id)
      challenge.opponent_correct_count = challenge.opponent_correct_count + 1
    end
    challenge.save
  end

  def handle_incorrect_challenge(game_detail, challenge)
    game_detail.correct_answers = 0
    game_detail.save
    if(current_user.id == challenge.player_id)
      challenge.player_incorrect_count = challenge.player_incorrect_count + 1
    elsif(current_user.id == challenge.opponent_id)
      challenge.opponent_incorrect_count = challenge.opponent_incorrect_count + 1
    end
    challenge.save
  end

  def handle_correct_answer(game_detail)
    game_detail.correct_answers= game_detail.correct_answers + 1
    if(game_detail.correct_answers > 3)
      game_detail.shown_crown_message = true
    end
    current_user.xp += GlobalConstants::REGULAR_QUESTION_XP_VALUE
    handle_player_leveling
    current_user.currency += GlobalConstants::REGULAR_QUESTION_CURRENCY_VALUE
    current_user.save
  end

  def handle_incorrect_answer(game, game_detail)
    game_detail.correct_answers = 0
    game_detail.save
    if(game.player_turn == game.player2_id)
      game.round= game.round + 1
    end
    game.player_turn = get_opponent(game).id
    game.save
  end

end
