class QuestionRatingController < ApplicationController

  def show
    @question = Question.where(id: params[:question_id]).first
  end

  def create
    user_id = params[:user_id]
    question_id = params[:question_id]
    rating = params[:rating]
    existing_rating = QuestionRating.where('user_id=? AND question_id=?', user_id, question_id).first
    if(existing_rating.nil? == true)
      QuestionRating.create(user_id: user_id, question_id: question_id, rating: rating)
    else
      existing_rating.rating = rating
      existing_rating.save
    end
  end


end
