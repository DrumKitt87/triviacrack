class ProfileController < ApplicationController

  def view
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      game_detail_ids = GameDetail.where(player_id: current_user.id).pluck(:id)
      @correct_answer_count = AnsweredQuestion.where('game_detail_id IN (?)', game_detail_ids).sum(:correct_count)
      @game_pieces_in_twos = GamePiece.all.each_slice(2)
    else
      redirect_to welcome_index_path
    end
  end

  private

  def get_level_up_percentage
    (((current_user.xp.to_f % GlobalConstants::LEVEL_UP_INTERVAL) / GlobalConstants::LEVEL_UP_INTERVAL.to_f) * 100.0).to_i
  end

  helper_method :get_level_up_percentage

  def get_category_name(category_id)
    Category.where(id: category_id).first.name
  end

  def get_percentage_by_category(category_id)
    game_detail_ids = GameDetail.where(player_id: current_user.id).pluck(:id)
    correct_category_answers = AnsweredQuestion.where('game_detail_id IN (?) AND category_id= ?', game_detail_ids, category_id).sum(:correct_count)
    total_category_answers = get_total_questions_by_category(category_id)
    if(total_category_answers > 0)
      percentage = (correct_category_answers.to_f / total_category_answers.to_f) * 100.0
    else
      percentage = 0.0
    end
    percentage.to_int
  end

  def get_total_questions_by_category(category_id)
    game_detail_ids = GameDetail.where(player_id: current_user.id).pluck(:id)
    correct_answers_count = AnsweredQuestion.where('game_detail_id IN (?) AND category_id= ?', game_detail_ids, category_id).sum(:correct_count)
    incorrect_answers_count = AnsweredQuestion.where('game_detail_id IN (?) AND category_id= ?', game_detail_ids, category_id).sum(:incorrect_count)
    total = correct_answers_count + incorrect_answers_count
    total
  end

  def get_number_of_games_won
    games_won = Game.where(winning_player_id: current_user.id)
    games_won.nil? ? 0 : games_won.count
  end

  def get_number_of_games_lost
    Game.where('(player1_id= ? OR player2_id= ?) AND winning_player_id != ?', current_user.id, current_user.id, current_user.id).count
  end

  def get_percentage_of_games_resigned
    games_resigned_count = Game.where('(player1_id= ? OR player2_id= ?) AND resigned_by_player_id != ?', current_user.id, current_user.id, current_user.id).count
    total_games_count = Game.where('player1_id= ? OR player2_id= ?', current_user.id, current_user.id).count
    if(total_games_count > 0)
      resigned_percentage = (games_resigned_count.to_f / total_games_count.to_f) * 100.0
    else
      resigned_percentage = 0
    end
    resigned_percentage.to_int
  end

  def get_number_of_challenges_played
    GameChallenge.where('player_id= ? OR opponent_id= ? AND winning_player_id IS NOT NULL', current_user.id, current_user.id).count
  end

  def get_number_of_challenges_won
    GameChallenge.where(winning_player_id: current_user.id).count
  end

  def get_number_of_challenges_lost
    get_number_of_challenges_played - get_number_of_challenges_won
  end

  helper_method :get_category_name,
                :get_percentage_by_category,
                :get_total_questions_by_category,
                :get_number_of_games_won,
                :get_number_of_games_lost,
                :get_percentage_of_games_resigned,
                :get_number_of_challenges_played,
                :get_number_of_challenges_won,
                :get_number_of_challenges_lost

end
