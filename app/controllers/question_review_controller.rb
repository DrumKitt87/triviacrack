class QuestionReviewController < ApplicationController

  ITEMS_PER_PAGE = 5

  def review
    if(current_user && current_user.role == GlobalConstants::REVIEWER)
      @current_page = params[:page_number] != nil ? params[:page_number].to_i : 0
      @total_records = Question.where(:active => false).count.to_f
      @total_pages = (@total_records / ITEMS_PER_PAGE.to_f).ceil
      @questions = Question.where(:active => false).order(created_at: :desc).offset(@current_page * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE)
    else
      redirect_to welcome_index_path
    end
  end

  def approve
    if(current_user && current_user.role == GlobalConstants::REVIEWER)
      question = Question.where(:id => params[:question_id]).first
      question.active = true
      if(question.save)
        flash[:notice] = 'Question approved successfully!'
      else
        flash[:error] = 'Question could not be approved successfully! Please try again later.'
      end
      redirect_to review_questions_path(params[:page_number])
    else
      redirect_to welcome_index_path
    end
  end

  def deny
    if(current_user && current_user.role == GlobalConstants::REVIEWER)
      if(Answer.destroy_all(:question_id => params[:question_id]) && Question.destroy_all(id: params[:question_id]))
        flash[:notice] = 'Question and associated answers removed'
        redirect_to review_questions_path(params[:page_number])
      end
    else
      redirect_to welcome_index_path
    end
  end

  private

  def get_answers(question_id)
    @answers = Answer.where(:question_id => question_id).order(:is_correct => :desc)
  end

  helper_method :get_answers

end
