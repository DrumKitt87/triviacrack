class PowerupController < ApplicationController

  def fifty_fifty
    @is_crown_or_challenge = params[:is_crown_or_challenge]
    cost = @is_crown_or_challenge == 'true' ? GlobalConstants::FIFTY_FIFTY_CROWN_COST : GlobalConstants::FIFTY_FIFTY_REGULAR_COST
    if(current_user.currency - cost > 0)
      current_user.currency -= cost
      current_user.save
    end
  end

  def extend_time
    @is_crown_or_challenge = params[:is_crown_or_challenge]
    cost = @is_crown_or_challenge == 'true' ? GlobalConstants::EXTEND_TIME_CROWN_COST : GlobalConstants::EXTEND_TIME_REGULAR_COST
    if(current_user.currency - cost > 0)
      current_user.currency -= cost
      current_user.save
    end
  end

  def skip_question
    @is_crown_or_challenge = (params[:is_crown] == 'true' || params[:is_challenge] == 'true')
    cost = @is_crown_or_challenge == true ? GlobalConstants::SKIP_QUESTION_CROWN_COST : GlobalConstants::SKIP_QUESTION_REGULAR_COST
    if(current_user.currency - cost > 0)
      current_user.currency -= cost
      current_user.save
    end
    redirect_to :controller => :question,
                :action => :answered_question,
                :game_id => params[:game_id],
                :question_id => params[:question_id],
                :answer_id => params[:answer_id],
                :is_crown => params[:is_crown],
                :is_challenge => params[:is_challenge],
                :challenge_id => params[:challenge_id],
                :did_not_answer => false,
                :power_up_skip => true
  end

end
