class SearchForOpponentsController < ApplicationController

  ITEMS_PER_PAGE = 10

  def select_opponent
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @keyword = params[:keyword] != nil ? params[:keyword] : ''
      @current_page = params[:page_number] != nil ? params[:page_number].to_i : 0
      all_players = User.where('id != ? AND role = ? AND (username LIKE ? OR email LIKE ?)', current_user.id, GlobalConstants::PLAYER, "%#{@keyword}%", "%#{@keyword}%")
      @total_records = all_players.count.to_f
      @total_pages = (@total_records / ITEMS_PER_PAGE.to_f).ceil
      @players = all_players.offset(@current_page * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE)

    else
      redirect_to welcome_index_path
    end
  end

  def create_game
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @game = Game.new
      @game.player1_id= current_user.id
      @game.player_turn = @game.player1_id
      @game.finished= false;

      current_player_game_details = GameDetail.new
      current_player_game_details.game = @game
      current_player_game_details.player_id = current_user.id

      @opponent = User.where(id: params[:opponent_id]).first
      @game.player2_id= @opponent.id

      opponent_game_details = GameDetail.new
      opponent_game_details.game = @game
      opponent_game_details.player_id = @opponent.id

      if(@game.save && opponent_game_details.save && current_player_game_details.save)
        redirect_to gameplay_spinner_path(@game)
      end
    else
      redirect_to welcome_index_path
    end
  end

end
