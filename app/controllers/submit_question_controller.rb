class SubmitQuestionController < ApplicationController


  def new
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @categories = Category.all
    else
      redirect_to welcome_index_path
    end
  end

  def create
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @question = Question.new
      @question.question_string = params[:question][:question_string]
      @question.category_id= params[:category_id]
      @question.difficulty_level= params[:difficulty_level]
      @question.active = false
      @question.reviewed = false

      @correct_answer = Answer.new
      @correct_answer.is_correct= true
      @correct_answer.answer_string = params[:correct_answer][:answer_string]

      @incorrect_answer_1 = Answer.new
      @incorrect_answer_1.is_correct= false
      @incorrect_answer_1.answer_string = params[:incorrect_answer_1][:answer_string]

      @incorrect_answer_2 = Answer.new
      @incorrect_answer_2.is_correct= false
      @incorrect_answer_2.answer_string = params[:incorrect_answer_2][:answer_string]

      @incorrect_answer_3 = Answer.new
      @incorrect_answer_3.is_correct= false
      @incorrect_answer_3.answer_string = params[:incorrect_answer_3][:answer_string]

      if(@question.save)
        @correct_answer.question_id = @question.id
        @incorrect_answer_1.question_id = @question.id
        @incorrect_answer_2.question_id = @question.id
        @incorrect_answer_3.question_id = @question.id
        if(@correct_answer.save && @incorrect_answer_1.save && @incorrect_answer_2.save && @incorrect_answer_3.save)
          flash[:notice] = 'Question submitted for review!'
          redirect_to home_index_path
        else
          flash[:error] = 'Question could not be submitted. Please try again later.'
        end
      else
        flash[:error] = 'Question could not be submitted. Please try again later.'
      end
    else
      redirect_to welcome_index_path
    end
  end


end
