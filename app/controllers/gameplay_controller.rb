class GameplayController < ApplicationController

  respond_to :html, :js

  include ApplicationHelper

  def spinner
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @game = Game.where(id: params[:id]).first
      @opponent = get_opponent(@game)
      @player_game_detail = @game.game_details.where(player_id: current_user.id).first
      @player_game_pieces = GameDetailGamePiece.where(game_detail_id: @player_game_detail.id).pluck(:game_piece_id)
      @opponent_game_detail = @game.game_details.where(player_id: @opponent.id).first
      @opponent_game_pieces = GameDetailGamePiece.where(game_detail_id: @opponent_game_detail.id).pluck(:game_piece_id)
      @game_pieces = GamePiece.all

      open_challenge = GameChallenge.where('game_id= ? AND winning_player_id IS NULL ', @game.id).first
      if(open_challenge != nil)
        flash[:notice] = open_challenge.player_id == current_user.id ? "Challenge In Progress" : "You've been challenged for a game piece"
        redirect_to challenge_next_question_path(@game.id, open_challenge.id)
      end
    else
      redirect_to welcome_index_path
    end
  end

  def spin
    @game_id = params[:game_id]
    @game = Game.where(id: @game_id).first
    @category_ids_array = Category.pluck(:id)
    if(rand(2) == 1)
      @category_ids_array.push(-1)
    end
    @random_category_id = @category_ids_array[rand(@category_ids_array.count)]
    @category_string = @random_category_id != -1 ? Category.where(id: @random_category_id).first.name : :crown
    @rotation = get_category_spinner_degrees(@category_string)
    @category_image_url = GamePiece.where(category_id: @random_category_id).first.image_url unless @random_category_id == -1
  end

  def choose_category
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @game = Game.where(id: params[:game_id]).first
      @game_detail = @game.game_details.where(player_id: current_user.id).first
      @player_game_pieces = GameDetailGamePiece.where(game_detail_id: @game_detail.id).pluck(:game_piece_id)
      @game_pieces_in_threes = GamePiece.all.each_slice(3)
    else
      redirect_to welcome_index_path
    end
  end

  def challenge_select_pieces
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @game = Game.where(id: params[:game_id]).first
      player_game_detail = @game.game_details.where(player_id: current_user.id).first
      @player_game_pieces = GameDetailGamePiece.where(game_detail_id: player_game_detail.id).pluck(:game_piece_id)
      opponent = get_opponent(@game)
      opponent_game_detail = @game.game_details.where(player_id: opponent.id).first
      opponent_game_pieces = GameDetailGamePiece.where(game_detail_id: opponent_game_detail.id).pluck(:game_piece_id)
      @game_pieces = GamePiece.all
      @eligible_game_pieces = opponent_game_pieces - @player_game_pieces
    else
      redirect_to welcome_index_path
    end

  end

  # TODO: Have challenge serve up the same questions to each player
  def challenge_setup
    opponent_game_piece_id = params[:opponent_category_id]
    wager_game_piece_id = params[:wager_category_id]

    game = Game.where(id: params[:game_id]).first
    player_game_detail = game.game_details.where(player_id: current_user.id).first
    opponent = get_opponent(game)
    opponent_game_detail = game.game_details.where(player_id: opponent.id).first

    challenge = GameChallenge.new
    challenge.game_id= game.id

    challenge.player_detail_id= player_game_detail.id
    challenge.player_id = current_user.id
    challenge.opponent_detail_id= opponent_game_detail.id
    challenge.opponent_id= opponent.id

    challenge.opponent_game_piece_id= opponent_game_piece_id
    challenge.wager_game_piece_id= wager_game_piece_id

    if(challenge.save)
      category_ids_array = Category.pluck(:id)
      random_category_id = category_ids_array[rand(category_ids_array.count)]
      redirect_to question_with_params_path(game.id, random_category_id, :false, :true, challenge.id)
    end
  end

  def challenge_next_question
    game = Game.where(id: params[:game_id]).first
    challenge = GameChallenge.where(id: params[:challenge_id]).first

    category_ids_array = Category.pluck(:id)
    random_category_id = category_ids_array[rand(category_ids_array.count)]

    if(challenge != nil)
      if(current_user.id == challenge.player_id)
        if(challenge.player_correct_count + challenge.player_incorrect_count < 6)
          redirect_to question_with_params_path(game.id, random_category_id, :false, :true, challenge.id)
        else
          game.player_turn = get_opponent(game).id
          if(game.save)
            flash[:notice] = "Your opponent will now partake in the challenge"
            redirect_to home_index_path
          end
        end
      elsif(current_user.id == challenge.opponent_id)
        if(challenge.opponent_correct_count + challenge.opponent_incorrect_count < 6)
          redirect_to question_with_params_path(game.id, random_category_id, :false, :true, challenge.id)
        elsif(challenge.opponent_correct_count == challenge.player_correct_count)
          flash[:notice] = 'Tie Breaker'
          redirect_to question_with_params_path(game.id, random_category_id, :false, :true, challenge.id)
        else
          if(challenge.player_correct_count > challenge.opponent_correct_count)
            opponent_game_piece = GameDetailGamePiece.where('game_detail_id=? AND game_piece_id=?', challenge.opponent_detail_id, challenge.opponent_game_piece_id).first
            if(opponent_game_piece != nil)
              opponent_game_piece.destroy
            end
            GameDetailGamePiece.create(game_detail_id: challenge.player_detail_id, game_piece_id: challenge.opponent_game_piece_id)
            challenge.winning_player_id= challenge.player_id
          else
            wager_game_piece = GameDetailGamePiece.where('game_detail_id=? AND game_piece_id=?', challenge.player_detail_id, challenge.wager_game_piece_id).first
            if(wager_game_piece != nil)
              wager_game_piece.destroy
            end
            challenge.winning_player_id= challenge.opponent_id
          end
          winning_player = User.where(id: challenge.winning_player_id).first
          if(winning_player.nil? == false)
            winning_player.xp += GlobalConstants::CHALLENGE_WON_XP_VALUE unless winning_player.nil? == true
            handle_player_leveling
            winning_player.currency += GlobalConstants::CHALLENGE_WON_CURRENCY_VALUE
            winning_player.save
          end
          game.player_turn = get_opponent(game).id
          if(game.save && challenge.save)
            redirect_to home_index_path
          end
        end
      end
    end
  end

  private

  ART_DEG = 2690
  ENTERTAINMENT_DEG = 3000
  GEOGRAPHY_DEG = 2900
  HISTORY_DEG = 2430
  SCIENCE_DEG = 2845
  SPORTS_DEG = 2740
  CROWN_DEG = 2946

  def get_category_spinner_degrees(category_string)
    case category_string
      when :art.to_s
        return ART_DEG
      when :entertainment.to_s
        return ENTERTAINMENT_DEG
      when :geography.to_s
        return GEOGRAPHY_DEG
      when :history.to_s
        return HISTORY_DEG
      when :science.to_s
        return SCIENCE_DEG
      when :sports.to_s
        return SPORTS_DEG
      when :crown.to_s
        return CROWN_DEG
      else
        return CROWN_DEG
    end
  end

  def challenge_available(player_game_pieces, opponent_game_pieces)
    opponent_game_pieces.count > 0 && player_game_pieces.count > 0 && (opponent_game_pieces - player_game_pieces).count > 0
  end

  helper_method :get_opponent, :challenge_available

end
