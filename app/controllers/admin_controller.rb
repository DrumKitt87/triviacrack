class AdminController < ApplicationController

  ITEMS_PER_PAGE = 10

  def admin
    if(current_user && current_user.role == GlobalConstants::ADMIN)
      @keyword = params[:keyword] != nil ? params[:keyword] : ''
      @current_page = params[:page_number] != nil ? params[:page_number].to_i : 0
      all_users = User.where('id != ? AND (username LIKE ? OR email LIKE ?)', current_user.id, "%#{@keyword}%", "%#{@keyword}%")
      @total_records = all_users.count.to_f
      @total_pages = (@total_records / ITEMS_PER_PAGE.to_f).ceil
      @users = all_users.offset(@current_page * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE)
    else
      redirect_to welcome_index_path
    end
  end

  def edit_user
    if(current_user && current_user.role == GlobalConstants::ADMIN)
      @user = User.where(id: params[:id]).first
      @user_roles = UserRole.all
    else
      redirect_to welcome_index_path
    end
  end

  def update_user
    if(current_user && current_user.role == GlobalConstants::ADMIN)
      user = User.where(id: params[:id]).first
      user.role = params[:user_role]
      if(user.save)
        flash[:notice] = 'User updated successfully!'
      else
        flash[:error] = 'Could not update user!'
      end
      redirect_to admin_path(0)
    else
      redirect_to welcome_index_path
    end
  end

end
