class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    if(current_user)
      if(current_user.role == GlobalConstants::ADMIN)
        admin_path(0)
      elsif(current_user.role == GlobalConstants::PLAYER)
        home_index_path
      elsif(current_user.role == GlobalConstants::REVIEWER)
        review_questions_path()
      end
    end
  end

  # before_filter :set_cache_buster
  #
  # def set_cache_buster
  #   response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
  #   response.headers["Pragma"] = "no-cache"
  #   response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  # end

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :username
  end

end
