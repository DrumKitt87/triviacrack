class HomeController < ApplicationController

  include ApplicationHelper

  def index
    if(current_user && current_user.role == GlobalConstants::PLAYER)
      @game_setup = GameSetup.new
      @game_setup.opponent_selection_type= :random

      @games = Game.where('player1_id= ? OR player2_id= ?', current_user.id, current_user.id).order('updated_at DESC')
      @your_turn_games, @their_turn_games, @finished_games = [], [], []
      @logos , @images = [], []
      @games.each do |game|
        @your_turn_games << game if game.player_turn == current_user.id && game.finished == false
        @their_turn_games << game if game.player_turn != current_user.id && game.finished == false
        @finished_games << game if game.finished == true
      end
    else
      redirect_to welcome_index_path
    end

  end

  def create_game_setup
    puts 'game_setup_params ' + game_setup_params.to_s
    @game_setup = GameSetup.new
    @game_setup.opponent_selection_type = game_setup_params[:opponent_selection_type]
    if @game_setup.save
      redirect_to setup_new_game_path(@game_setup)
    else
      flash[:error] = 'Could not create new game'
      render 'index'
    end
  end

  def setup_new_game
    @opponent_selection_type = GameSetup.where(id: params[:id]).first.opponent_selection_type
    GameSetup.where(id: params[:id]).first.destroy

    if(User.where(role: GlobalConstants::PLAYER).count < 2)
      flash[:error] = 'More players must join before you can start a match'
      redirect_to home_index_path
    elsif(@opponent_selection_type == 'random')
      @game = Game.new
      @game.player1_id= current_user.id
      @game.player2_id = pick_random_opponent
      @game.player_turn = @game.player1_id
      @game.finished= false;

      @opponent = get_opponent @game

      # Create Game Details for both players
      current_player_game_details = GameDetail.new
      current_player_game_details.game = @game
      current_player_game_details.player_id = current_user.id
      current_player_game_details.save

      opponent_game_details = GameDetail.new
      opponent_game_details.game = @game
      opponent_game_details.player_id = @opponent.id
      opponent_game_details.save

      if(@game.save)
        redirect_to gameplay_spinner_path(@game)
      end
    elsif @opponent_selection_type == 'friends'
      redirect_to select_opponent_path(0)
    end
  end

  private

  def game_setup_params
    params.require(:game_setup).permit(:game_setup, :opponent_selection_type)
  end

  def pick_random_opponent
    @user_ids_array = User.where("id != ? AND role= ?", current_user.id, GlobalConstants::PLAYER).collect(&:id)
    @random_user_id = @user_ids_array[rand(@user_ids_array.size)]
    @random_user_id
  end

  def get_user_score game
    @game_detail_id = game.game_details.where(player_id: current_user.id).first.id
    GameDetailGamePiece.where(game_detail_id: @game_detail_id).count
  end

  def get_opponent_score game
    @opponent = get_opponent(game)
    @game_detail_id = game.game_details.where(player_id: @opponent.id).first.id
    GameDetailGamePiece.where(game_detail_id: @game_detail_id).count
  end

  helper_method :get_opponent, :get_user_score, :get_opponent_score

end
