require 'test_helper'

class AdminControllerTest < ActionController::TestCase

  test 'should allow user with admin role to access admin' do
    sign_in(users(:admin))
    get :admin
    assert_response :success
  end

  test 'should redirect when user with non-admin role tries to access admin' do
    get :admin
    assert_redirected_to welcome_index_path

    sign_in(users(:reviewer))
    get :admin
    assert_redirected_to welcome_index_path

    sign_out(users(:reviewer))

    sign_in(users(:player1))
    get :admin
    assert_redirected_to welcome_index_path
  end

  test 'should get user_to_change when edit_user is called' do
    sign_in(users(:admin))
    get :edit_user, id: users(:user_to_change).id
    assert_not_nil assigns(:user)
  end

  test 'should update user role to 1 to 2 when update user is called by admin role' do
    sign_in(users(:admin))
    assert_equal 2, User.where(id: users(:user_to_change).id).first.role
    post :update_user, id: users(:user_to_change).id, user_role: 1
    assert_equal 1, User.where(id: users(:user_to_change).id).first.role
    assert_redirected_to admin_path(0)
  end

  test 'should redirect to welcome when update user is called by non-admin role' do
    post :update_user, id: users(:user_to_change).id, user_role: 1
    assert_redirected_to welcome_index_path

    sign_in(users(:player1))
    post :update_user, id: users(:user_to_change).id, user_role: 1
    assert_redirected_to welcome_index_path

    sign_out(users(:player1))

    sign_in(users(:reviewer))
    post :update_user, id: users(:user_to_change).id, user_role: 1
    assert_redirected_to welcome_index_path
  end

end
