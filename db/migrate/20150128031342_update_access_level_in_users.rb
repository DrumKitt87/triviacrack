class UpdateAccessLevelInUsers < ActiveRecord::Migration
  def change
    remove_column :users, :access_level
    add_column :users, :role, :string
  end
end
