class AddColumnsToGameChallenges < ActiveRecord::Migration
  def change
    rename_column :game_challenges, :game_detail1_id, :player_detail_id
    rename_column :game_challenges, :game_detail2_id, :opponent_detail_id
    add_column :game_challenges, :opponent_game_piece_id, :integer
    add_column :game_challenges, :wager_game_piece_id, :integer
  end
end
