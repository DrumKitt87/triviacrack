class RemoveColumnsFromUserTable < ActiveRecord::Migration
  def change
    remove_column :users, :xp
    remove_column :users, :level
    remove_column :users, :questions_correct
  end
end
