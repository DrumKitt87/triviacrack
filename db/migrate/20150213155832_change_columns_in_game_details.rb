class ChangeColumnsInGameDetails < ActiveRecord::Migration
  def change
    remove_column :game_details, :questions_correct
  end
end
