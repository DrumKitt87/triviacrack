class CreateUserRoles < ActiveRecord::Migration
  def change
    drop_table :user_roles
    create_table :user_roles do |t|
      t.integer :constant_id
      t.string :name
      t.timestamps
    end
  end
end
