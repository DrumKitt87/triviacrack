class CreateQuestionRatings < ActiveRecord::Migration
  def change
    create_table :question_ratings do |t|
      t.references :question, index: true
      t.references :user, index: true
      t.integer :rating

      t.timestamps
    end
  end
end
