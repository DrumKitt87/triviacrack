class AddDefaultsToGameChallenges < ActiveRecord::Migration
  def change
    change_column :game_challenges, :game_detail1_correct, :integer, :default => 0
    change_column :game_challenges, :game_detail1_incorrect, :integer, :default => 0
    change_column :game_challenges, :game_detail2_correct, :integer, :default => 0
    change_column :game_challenges, :game_detail2_incorrect, :integer, :default => 0
  end
end
