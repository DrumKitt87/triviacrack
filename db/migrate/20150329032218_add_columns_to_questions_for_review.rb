class AddColumnsToQuestionsForReview < ActiveRecord::Migration
  def change
    add_column :questions, :reviewed, :boolean, :default => false
  end
end
