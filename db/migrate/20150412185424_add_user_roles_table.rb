class AddUserRolesTable < ActiveRecord::Migration
  def change
    create_table :user_roles
    add_column :user_roles, :constant_id, :integer
    add_column :user_roles, :name, :string
  end
end