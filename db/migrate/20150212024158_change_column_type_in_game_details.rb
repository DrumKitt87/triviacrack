class ChangeColumnTypeInGameDetails < ActiveRecord::Migration
  def change
    change_column :game_details, :game_pieces_collected, :text, array: true, default: []
  end
end
