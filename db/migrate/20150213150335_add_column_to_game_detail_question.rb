class AddColumnToGameDetailQuestion < ActiveRecord::Migration
  def change
    add_column :game_detail_questions, :question_answered, :boolean
  end
end
