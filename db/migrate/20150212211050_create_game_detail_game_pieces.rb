class CreateGameDetailGamePieces < ActiveRecord::Migration
  def change
    create_table :game_detail_game_pieces do |t|
      t.timestamps
      t.integer :game_detail_id
      t.integer :game_piece_id
    end
  end
end
