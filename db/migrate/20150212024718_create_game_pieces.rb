class CreateGamePieces < ActiveRecord::Migration
  def change
    create_table :game_pieces do |t|
      t.integer :category_id
      t.string :image_url

      t.timestamps
    end
  end
end
