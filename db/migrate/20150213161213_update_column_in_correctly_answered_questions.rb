class UpdateColumnInCorrectlyAnsweredQuestions < ActiveRecord::Migration
  def change
    change_column :correctly_answered_questions, :count, :integer, :default => 1
  end
end
