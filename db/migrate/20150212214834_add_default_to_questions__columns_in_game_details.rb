class AddDefaultToQuestionsColumnsInGameDetails < ActiveRecord::Migration
  def change
    change_column :game_details, :questions_correct, :integer, :default => 0
    change_column :game_details, :questions_incorrect, :integer, :default => 0
  end
end
