class CreateGameDetailQuestions < ActiveRecord::Migration
  def change
    create_table :game_detail_questions do |t|
      t.integer :game_detail_id
      t.integer :question_id

      t.timestamps
    end
  end
end
