class ReAddColumnToGameDetails < ActiveRecord::Migration
  def change
    add_column :game_details, :correct_answers, :integer, default: 0
    remove_column :game_details, :questions_incorrect
  end
end
