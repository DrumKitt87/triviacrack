class CreateGameChallengeQuestions < ActiveRecord::Migration
  def change
    create_table :game_challenge_questions do |t|
      t.integer :game_challenge_id
      t.integer :game_question_id

      t.timestamps
    end
  end
end
