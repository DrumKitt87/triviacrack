class RemoveColumnTypeFromGameDetails < ActiveRecord::Migration
  def change
    remove_column :game_details, :game_pieces_collected
  end
end
