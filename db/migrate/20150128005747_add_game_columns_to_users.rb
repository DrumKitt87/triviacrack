class AddGameColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :xp,  :integer, :default => 0
    add_column :users, :level, :integer, :default => 1
    add_column :users, :currency, :integer, :default => 0
    add_column :users, :questions_correct, :integer, :default => 0
    add_column :users, :access_level, :integer
  end
end
