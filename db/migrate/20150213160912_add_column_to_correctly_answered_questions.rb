class AddColumnToCorrectlyAnsweredQuestions < ActiveRecord::Migration
  def change
    add_column :correctly_answered_questions, :count, :integer, :default => 0
  end
end
