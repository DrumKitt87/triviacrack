class AddColumnsToGamedetails < ActiveRecord::Migration
  def change
    add_column :game_details, :game_pieces_collected, :integer, array: true, default: []
  end
end
