class MoveWinningPlayerIdToGameTable < ActiveRecord::Migration
  def change
    remove_column :game_details, :winning_player_id
    add_column :games, :winning_player_id, :integer
  end
end
