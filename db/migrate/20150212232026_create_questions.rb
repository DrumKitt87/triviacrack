class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :category, index: true
      t.integer :difficulty_level

      t.timestamps
    end
  end
end
