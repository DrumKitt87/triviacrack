class AddColumnsToQuestionAndAnswer < ActiveRecord::Migration
  def change
    add_column :questions, :question_string, :string
    add_column :answers, :answer_string, :string
  end
end
