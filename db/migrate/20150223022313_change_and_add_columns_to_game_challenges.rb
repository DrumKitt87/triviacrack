class ChangeAndAddColumnsToGameChallenges < ActiveRecord::Migration
  def change
    rename_column :game_challenges, :game_detail1_correct, :player_correct_count
    rename_column :game_challenges, :game_detail1_incorrect, :player_incorrect_count
    rename_column :game_challenges, :game_detail2_correct, :opponent_correct_count
    rename_column :game_challenges, :game_detail2_incorrect, :opponent_incorrect_count
  end
end
