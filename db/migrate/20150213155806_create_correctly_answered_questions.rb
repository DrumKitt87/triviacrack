class CreateCorrectlyAnsweredQuestions < ActiveRecord::Migration
  def change
    create_table :correctly_answered_questions do |t|
      t.integer :game_detail_id
      t.references :category, index: true

      t.timestamps
    end
  end
end
