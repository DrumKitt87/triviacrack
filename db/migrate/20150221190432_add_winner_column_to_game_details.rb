class AddWinnerColumnToGameDetails < ActiveRecord::Migration
  def change
    add_column :game_details, :winning_player_id, :integer
  end
end
