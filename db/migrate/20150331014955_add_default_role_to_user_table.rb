class AddDefaultRoleToUserTable < ActiveRecord::Migration
  def change
    change_column_default :users, :role, 'player'
  end
end
