class CreateGameDetails < ActiveRecord::Migration
  def change
    create_table :game_details do |t|
      t.references :game, index: true
      t.integer :player_id
      t.integer :questions_correct
      t.integer :questions_incorrect

      t.timestamps
    end
  end
end
