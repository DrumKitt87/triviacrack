class CreateGameChallenges < ActiveRecord::Migration
  def change
    create_table :game_challenges do |t|
      t.integer :game_detail1_id
      t.integer :game_detail2_id
      t.integer :game_detail1_correct
      t.integer :game_detail1_incorrect
      t.integer :game_detail2_correct
      t.integer :game_detail2_incorrect
      t.integer :winning_player_id

      t.timestamps
    end
  end
end
