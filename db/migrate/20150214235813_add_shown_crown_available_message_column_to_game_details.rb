class AddShownCrownAvailableMessageColumnToGameDetails < ActiveRecord::Migration
  def change
    add_column :game_details, :shown_crown_message, :boolean, :default => false
  end
end
