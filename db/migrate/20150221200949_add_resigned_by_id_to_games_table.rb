class AddResignedByIdToGamesTable < ActiveRecord::Migration
  def change
    add_column :games, :resigned_by_player_id, :integer
  end
end
