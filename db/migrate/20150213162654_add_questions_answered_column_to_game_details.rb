class AddQuestionsAnsweredColumnToGameDetails < ActiveRecord::Migration
  def change
    add_column :game_details, :questions_answered_in_turn, :integer, default: 0
    add_column :game_details, :incorrect_answers, :integer, default: 0
  end
end
