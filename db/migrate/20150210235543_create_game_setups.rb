class CreateGameSetups < ActiveRecord::Migration
  def change
    create_table :game_setups do |t|
      t.string :opponent_selection_type

      t.timestamps
    end
  end
end
