class RenameCorrectlyAnsweredQuestionToAnsweredQuestion < ActiveRecord::Migration
  def change
    change_column :correctly_answered_questions, :count, :integer, :default => 0
    rename_column :correctly_answered_questions, :count, :correct_count
    add_column :correctly_answered_questions, :incorrect_count, :integer, :default => 0
    rename_table :correctly_answered_questions, :answered_questions
  end
end
