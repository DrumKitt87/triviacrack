class MoveColumnsBackIntoUserTable < ActiveRecord::Migration
  def change
    add_column :users, :xp, :integer, default: 0
    add_column :users, :level, :integer, default: 1
    change_column :users, :currency, :integer, default: 0
  end
end
