class AddPlayerIdColumnsToGameChallenges < ActiveRecord::Migration
  def change
    add_column :game_challenges, :player_id, :integer
    add_column :game_challenges, :opponent_id, :integer
  end
end
