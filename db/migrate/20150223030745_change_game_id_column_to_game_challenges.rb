class ChangeGameIdColumnToGameChallenges < ActiveRecord::Migration
  def change
    add_column :game_challenges, :game_id, :integer
  end
end
