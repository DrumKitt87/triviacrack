
# <editor-fold desc="UserRoles">

UserRole.create(:constant_id => 0, :name => 'admin')
UserRole.create(:constant_id => 1, :name => 'reviewer')
UserRole.create(:constant_id => 2, :name => 'player')

# </editor-fold desc="UserRoles">

# <editor-fold desc="Logins">
#admin login
User.create(email:'admin@example.com', username: 'admin', password: "password", role: GlobalConstants::ADMIN)
User.create(email:'reviewer@example.com', username: 'reviewer', password: "password", role: GlobalConstants::REVIEWER)
User.create(email:'player1@example.com', username: 'player1', password: "password", role: GlobalConstants::PLAYER)
User.create(email:'player2@example.com', username: 'player2', password: "password", role: GlobalConstants::PLAYER)
User.create(email:'player3@example.com', username: 'player3', password: "password", role: GlobalConstants::PLAYER)
# </editor-fold>

# <editor-fold desc="Categories">
artCategory = Category.create(name: :art)
entertainmentCategory = Category.create(name: :entertainment)
geographyCategory = Category.create(name: :geography)
historyCategory = Category.create(name: :history)
scienceCategory = Category.create(name: :science)
sportsCategory = Category.create(name: :sports)

artCategory.save!
entertainmentCategory.save!
geographyCategory.save!
historyCategory.save!
scienceCategory.save!
sportsCategory.save!

# </editor-fold>

# <editor-fold desc="GamePieces">
artGamePiece = GamePiece.create(category_id: Category.where(name: :art).first.id)
artGamePiece.image_url = 'CategoryIcons/art.jpg'
artGamePiece.save!

entertainmentGamePiece = GamePiece.create(category_id: Category.where(name: :entertainment).first.id)
entertainmentGamePiece.image_url = 'CategoryIcons/entertainment.jpg'
entertainmentGamePiece.save!

geographyGamePiece = GamePiece.create(category_id: Category.where(name: :geography).first.id)
geographyGamePiece.image_url = 'CategoryIcons/geography.jpg'
geographyGamePiece.save!

historyGamePiece = GamePiece.create(category_id: Category.where(name: :history).first.id)
historyGamePiece.image_url = 'CategoryIcons/history.jpg'
historyGamePiece.save!

scienceGamePiece = GamePiece.create(category_id: Category.where(name: :science).first.id)
scienceGamePiece.image_url = 'CategoryIcons/science.jpg'
scienceGamePiece.save!

sportsGamePiece = GamePiece.create(category_id: Category.where(name: :sports).first.id)
sportsGamePiece.image_url = 'CategoryIcons/sports.jpg'
sportsGamePiece.save!
# </editor-fold>

# <editor-fold desc="Question Template">
# art_q1 = Question.create(question_string: )
# art_q1.category_id = Category.where(name: :art).first.id
# art_q1.difficulty_level= 1
# art_q1.save
#
# art_q1_correct_answer = Answer.create(answer_string: )
# art_q1_correct_answer.question_id= art_q1.id
# art_q1_correct_answer.is_correct= true
# art_q1_correct_answer.save
#
# art_q1_answer2 = Answer.create(answer_string: )
# art_q1_answer2.question_id= art_q1.id
# art_q1_answer2.is_correct= false
# art_q1_answer2.save
#
# art_q1_answer3 = Answer.create(answer_string: )
# art_q1_answer3.question_id= art_q1.id
# art_q1_answer3.is_correct= false
# art_q1_answer3.save
#
# art_q1_answer4 = Answer.create(answer_string: )
# art_q1_answer4.question_id= art_q1.id
# art_q1_answer4.is_correct= false
# art_q1_answer4.save
# </editor-fold>

# <editor-fold desc="Art Questions">
art_q1 = Question.create(question_string: "During the mid 20th century, most of the works of American painter Norman Rockwell appeared as cover illustrations for what magazine?")
art_q1.category_id = Category.where(name: :art).first.id
art_q1.difficulty_level= 1
art_q1.save

art_q1_correct_answer = Answer.create(answer_string: 'The Saturday Evening Post')
art_q1_correct_answer.question_id= art_q1.id
art_q1_correct_answer.is_correct= true
art_q1_correct_answer.save

art_q1_answer2 = Answer.create(answer_string: "The Artist's Magazine")
art_q1_answer2.question_id= art_q1.id
art_q1_answer2.is_correct= false
art_q1_answer2.save

art_q1_answer3 = Answer.create(answer_string: 'Art & Project')
art_q1_answer3.question_id= art_q1.id
art_q1_answer3.is_correct= false
art_q1_answer3.save

art_q1_answer4 = Answer.create(answer_string: 'Flash Art')
art_q1_answer4.question_id= art_q1.id
art_q1_answer4.is_correct= false
art_q1_answer4.save



art_q2 = Question.create(question_string: "Who is credited as the designer of the many statues which decorated the Parthenon?")
art_q2.category_id = Category.where(name: :art).first.id
art_q2.difficulty_level= 1
art_q2.save

art_q2_correct_answer = Answer.create(answer_string: 'Phidias')
art_q2_correct_answer.question_id= art_q2.id
art_q2_correct_answer.is_correct= true
art_q2_correct_answer.save

art_q2_answer2 = Answer.create(answer_string: "Jebodia")
art_q2_answer2.question_id= art_q2.id
art_q2_answer2.is_correct= false
art_q2_answer2.save

art_q2_answer3 = Answer.create(answer_string: 'Scopas')
art_q2_answer3.question_id= art_q2.id
art_q2_answer3.is_correct= false
art_q2_answer3.save

art_q2_answer4 = Answer.create(answer_string: 'Hesiod')
art_q2_answer4.question_id= art_q2.id
art_q2_answer4.is_correct= false
art_q2_answer4.save


art_q3 = Question.create(question_string: 'What artist was struck in the face with a mallet by an envious rival, disfiguring him for life?')
art_q3.category_id = Category.where(name: :art).first.id
art_q3.difficulty_level= 1
art_q3.save

art_q3_correct_answer = Answer.create(answer_string: 'Michelangelo')
art_q3_correct_answer.question_id= art_q3.id
art_q3_correct_answer.is_correct= true
art_q3_correct_answer.save

art_q3_answer2 = Answer.create(answer_string: 'Raphael')
art_q3_answer2.question_id= art_q3.id
art_q3_answer2.is_correct= false
art_q3_answer2.save

art_q3_answer3 = Answer.create(answer_string: 'Titian')
art_q3_answer3.question_id= art_q3.id
art_q3_answer3.is_correct= false
art_q3_answer3.save

art_q3_answer4 = Answer.create(answer_string: 'Rembrandt')
art_q3_answer4.question_id= art_q3.id
art_q3_answer4.is_correct= false
art_q3_answer4.save


art_q4 = Question.create(question_string: "What landmark work came about from a friend's request for a self-portrait of Rene Magritte?")
art_q4.category_id = Category.where(name: :art).first.id
art_q4.difficulty_level= 1
art_q4.save

art_q4_correct_answer = Answer.create(answer_string: 'The Son of Man')
art_q4_correct_answer.question_id= art_q4.id
art_q4_correct_answer.is_correct= true
art_q4_correct_answer.save

art_q4_answer2 = Answer.create(answer_string: 'Black Magic')
art_q4_answer2.question_id= art_q4.id
art_q4_answer2.is_correct= false
art_q4_answer2.save

art_q4_answer3 = Answer.create(answer_string: 'Evil of the Country')
art_q4_answer3.question_id= art_q4.id
art_q4_answer3.is_correct= false
art_q4_answer3.save

art_q4_answer4 = Answer.create(answer_string: 'The Red Model')
art_q4_answer4.question_id= art_q4.id
art_q4_answer4.is_correct= false
art_q4_answer4.save


art_q5 = Question.create(question_string: 'Who was the court sculptor of Alexander the Great?')
art_q5.category_id = Category.where(name: :art).first.id
art_q5.difficulty_level= 1
art_q5.save

art_q5_correct_answer = Answer.create(answer_string: 'Lysippus')
art_q5_correct_answer.question_id= art_q5.id
art_q5_correct_answer.is_correct= true
art_q5_correct_answer.save

art_q5_answer2 = Answer.create(answer_string: 'Vitruvius')
art_q5_answer2.question_id= art_q5.id
art_q5_answer2.is_correct= false
art_q5_answer2.save

art_q5_answer3 = Answer.create(answer_string: 'Praxiteles')
art_q5_answer3.question_id= art_q5.id
art_q5_answer3.is_correct= false
art_q5_answer3.save

art_q5_answer4 = Answer.create(answer_string: 'Appeles')
art_q5_answer4.question_id= art_q5.id
art_q5_answer4.is_correct= false
art_q5_answer4.save



art_q6 = Question.create(question_string: "What French author wrote the plays ‘Dirty Hands’ and ‘The Respectful Prostitute’?")
art_q6.category_id = Category.where(name: :art).first.id
art_q6.difficulty_level= 1
art_q6.save

art_q6_correct_answer = Answer.create(answer_string: 'Jean Paul Satre')
art_q6_correct_answer.question_id= art_q6.id
art_q6_correct_answer.is_correct= true
art_q6_correct_answer.save

art_q6_answer2 = Answer.create(answer_string: 'Gustave Flaubert')
art_q6_answer2.question_id= art_q6.id
art_q6_answer2.is_correct= false
art_q6_answer2.save

art_q6_answer3 = Answer.create(answer_string: 'Victor Hugo')
art_q6_answer3.question_id= art_q6.id
art_q6_answer3.is_correct= false
art_q6_answer3.save

art_q6_answer4 = Answer.create(answer_string: 'None of these')
art_q6_answer4.question_id= art_q6.id
art_q6_answer4.is_correct= false
art_q6_answer4.save



art_q7 = Question.create(question_string: "Who wrote the play ‘Waiting for Godot’?")
art_q7.category_id = Category.where(name: :art).first.id
art_q7.difficulty_level= 1
art_q7.save

art_q7_correct_answer = Answer.create(answer_string: 'Sumuel Beckett')
art_q7_correct_answer.question_id= art_q7.id
art_q7_correct_answer.is_correct= true
art_q7_correct_answer.save

art_q7_answer2 = Answer.create(answer_string: 'Tennessee Williams')
art_q7_answer2.question_id= art_q7.id
art_q7_answer2.is_correct= false
art_q7_answer2.save

art_q7_answer3 = Answer.create(answer_string: 'William Shakespeare')
art_q7_answer3.question_id= art_q7.id
art_q7_answer3.is_correct= false
art_q7_answer3.save

art_q7_answer4 = Answer.create(answer_string: 'Gregorio de la Ferrere')
art_q7_answer4.question_id= art_q7.id
art_q7_answer4.is_correct= false
art_q7_answer4.save



art_q8 = Question.create(question_string: 'Who wrote the symphony number 40 (Requiem)?')
art_q8.category_id = Category.where(name: :art).first.id
art_q8.difficulty_level= 1
art_q8.save

art_q8_correct_answer = Answer.create(answer_string: 'Mozart')
art_q8_correct_answer.question_id= art_q8.id
art_q8_correct_answer.is_correct= true
art_q8_correct_answer.save

art_q8_answer2 = Answer.create(answer_string: 'Choppin')
art_q8_answer2.question_id= art_q8.id
art_q8_answer2.is_correct= false
art_q8_answer2.save

art_q8_answer3 = Answer.create(answer_string: 'Beethoven')
art_q8_answer3.question_id= art_q8.id
art_q8_answer3.is_correct= false
art_q8_answer3.save

art_q8_answer4 = Answer.create(answer_string: 'Stratocaster')
art_q8_answer4.question_id= art_q8.id
art_q8_answer4.is_correct= false
art_q8_answer4.save



art_q9 = Question.create(question_string: 'Who wrote the book of tales “Shadows of the night”?')
art_q9.category_id = Category.where(name: :art).first.id
art_q9.difficulty_level= 1
art_q9.save

art_q9_correct_answer = Answer.create(answer_string: 'Stephen King')
art_q9_correct_answer.question_id= art_q9.id
art_q9_correct_answer.is_correct= true
art_q9_correct_answer.save

art_q9_answer2 = Answer.create(answer_string: 'J. K. Rowling')
art_q9_answer2.question_id= art_q9.id
art_q9_answer2.is_correct= false
art_q9_answer2.save

art_q9_answer3 = Answer.create(answer_string: 'George R. R. Martin')
art_q9_answer3.question_id= art_q9.id
art_q9_answer3.is_correct= false
art_q9_answer3.save

art_q9_answer4 = Answer.create(answer_string: 'Rick Riordan')
art_q9_answer4.question_id= art_q9.id
art_q9_answer4.is_correct= false
art_q9_answer4.save



art_q10 = Question.create(question_string: "Who wrote the ‘Foundation’ saga?")
art_q10.category_id = Category.where(name: :art).first.id
art_q10.difficulty_level= 1
art_q10.save

art_q10_correct_answer = Answer.create(answer_string: 'Isaac Asimov')
art_q10_correct_answer.question_id= art_q10.id
art_q10_correct_answer.is_correct= true
art_q10_correct_answer.save

art_q10_answer2 = Answer.create(answer_string: 'Ken Follet')
art_q10_answer2.question_id= art_q10.id
art_q10_answer2.is_correct= false
art_q10_answer2.save

art_q10_answer3 = Answer.create(answer_string: 'T. Benacquista')
art_q10_answer3.question_id= art_q10.id
art_q10_answer3.is_correct= false
art_q10_answer3.save

art_q10_answer4 = Answer.create(answer_string: 'Julio Verne')
art_q10_answer4.question_id= art_q10.id
art_q10_answer4.is_correct= false
art_q10_answer4.save



art_q11 = Question.create(question_string: 'What type of color is green?')
art_q11.category_id = Category.where(name: :art).first.id
art_q11.difficulty_level= 1
art_q11.save

art_q11_correct_answer = Answer.create(answer_string: 'Secondary')
art_q11_correct_answer.question_id= art_q11.id
art_q11_correct_answer.is_correct= true
art_q11_correct_answer.save

art_q11_answer2 = Answer.create(answer_string: 'Shade')
art_q11_answer2.question_id= art_q11.id
art_q11_answer2.is_correct= false
art_q11_answer2.save

art_q11_answer3 = Answer.create(answer_string: 'Primary')
art_q11_answer3.question_id= art_q11.id
art_q11_answer3.is_correct= false
art_q11_answer3.save

art_q11_answer4 = Answer.create(answer_string: 'Tint')
art_q11_answer4.question_id= art_q11.id
art_q11_answer4.is_correct= false
art_q11_answer4.save



art_q12 = Question.create(question_string: 'How many letters are in the english alphabet?')
art_q12.category_id = Category.where(name: :art).first.id
art_q12.difficulty_level= 1
art_q12.save

art_q12_correct_answer = Answer.create(answer_string: '26')
art_q12_correct_answer.question_id= art_q12.id
art_q12_correct_answer.is_correct= true
art_q12_correct_answer.save

art_q12_answer2 = Answer.create(answer_string: '28')
art_q12_answer2.question_id= art_q12.id
art_q12_answer2.is_correct= false
art_q12_answer2.save

art_q12_answer3 = Answer.create(answer_string: '27')
art_q12_answer3.question_id= art_q12.id
art_q12_answer3.is_correct= false
art_q12_answer3.save

art_q12_answer4 = Answer.create(answer_string: '29')
art_q12_answer4.question_id= art_q12.id
art_q12_answer4.is_correct= false
art_q12_answer4.save
# </editor-fold>

# <editor-fold desc="Entertainment Questions">
entertainment_q1 = Question.create(question_string: "Who was Little Orphan Annie's benefactor?")
entertainment_q1.category_id = Category.where(name: :entertainment).first.id
entertainment_q1.difficulty_level= 1
entertainment_q1.save

entertainment_q1_correct_answer = Answer.create(answer_string: "Daddy Warbucks")
entertainment_q1_correct_answer.question_id= entertainment_q1.id
entertainment_q1_correct_answer.is_correct= true
entertainment_q1_correct_answer.save

entertainment_q1_answer2 = Answer.create(answer_string: "Little Jimmy")
entertainment_q1_answer2.question_id= entertainment_q1.id
entertainment_q1_answer2.is_correct= false
entertainment_q1_answer2.save

entertainment_q1_answer3 = Answer.create(answer_string: 'Orphan Timmy')
entertainment_q1_answer3.question_id= entertainment_q1.id
entertainment_q1_answer3.is_correct= false
entertainment_q1_answer3.save

entertainment_q1_answer4 = Answer.create(answer_string: 'Elvis Costello')
entertainment_q1_answer4.question_id= entertainment_q1.id
entertainment_q1_answer4.is_correct= false
entertainment_q1_answer4.save


entertainment_q2 = Question.create(question_string: 'What Hollywood power couple announced they were separating on January 7, 2005?')
entertainment_q2.category_id = Category.where(name: :entertainment).first.id
entertainment_q2.difficulty_level= 1
entertainment_q2.save

entertainment_q2_correct_answer = Answer.create(answer_string: 'Brad Pitt & Jennifer Aniston')
entertainment_q2_correct_answer.question_id= entertainment_q2.id
entertainment_q2_correct_answer.is_correct= true
entertainment_q2_correct_answer.save

entertainment_q2_answer2 = Answer.create(answer_string: 'Tom Cruise & Nicole Kidman')
entertainment_q2_answer2.question_id= entertainment_q2.id
entertainment_q2_answer2.is_correct= false
entertainment_q2_answer2.save

entertainment_q2_answer3 = Answer.create(answer_string: 'John Travolta & Kelly Preston')
entertainment_q2_answer3.question_id= entertainment_q2.id
entertainment_q2_answer3.is_correct= false
entertainment_q2_answer3.save

entertainment_q2_answer4 = Answer.create(answer_string: 'Tim Robbins & Susan Sarandon')
entertainment_q2_answer4.question_id= entertainment_q2.id
entertainment_q2_answer4.is_correct= false
entertainment_q2_answer4.save


entertainment_q3 = Question.create(question_string: 'Who was the first major American celebrity whose AIDS diagnosis became public knowledge?')
entertainment_q3.category_id = Category.where(name: :entertainment).first.id
entertainment_q3.difficulty_level= 1
entertainment_q3.save

entertainment_q3_correct_answer = Answer.create(answer_string: 'Rock Hudson')
entertainment_q3_correct_answer.question_id= entertainment_q3.id
entertainment_q3_correct_answer.is_correct= true
entertainment_q3_correct_answer.save

entertainment_q3_answer2 = Answer.create(answer_string: 'Clark Gable')
entertainment_q3_answer2.question_id= entertainment_q3.id
entertainment_q3_answer2.is_correct= false
entertainment_q3_answer2.save

entertainment_q3_answer3 = Answer.create(answer_string: 'Magic Johnson')
entertainment_q3_answer3.question_id= entertainment_q3.id
entertainment_q3_answer3.is_correct= false
entertainment_q3_answer3.save

entertainment_q3_answer4 = Answer.create(answer_string: 'Isaac Asimov')
entertainment_q3_answer4.question_id= entertainment_q3.id
entertainment_q3_answer4.is_correct= false
entertainment_q3_answer4.save


entertainment_q4 = Question.create(question_string: 'What professional basketball player did Madonna have an affair with in the mid 1990s?')
entertainment_q4.category_id = Category.where(name: :entertainment).first.id
entertainment_q4.difficulty_level= 1
entertainment_q4.save

entertainment_q4_correct_answer = Answer.create(answer_string: 'Dennis Rodman')
entertainment_q4_correct_answer.question_id= entertainment_q4.id
entertainment_q4_correct_answer.is_correct= true
entertainment_q4_correct_answer.save

entertainment_q4_answer2 = Answer.create(answer_string: 'Michael Jordan')
entertainment_q4_answer2.question_id= entertainment_q4.id
entertainment_q4_answer2.is_correct= false
entertainment_q4_answer2.save

entertainment_q4_answer3 = Answer.create(answer_string: "Shaquille O'Neal")
entertainment_q4_answer3.question_id= entertainment_q4.id
entertainment_q4_answer3.is_correct= false
entertainment_q4_answer3.save

entertainment_q4_answer4 = Answer.create(answer_string: 'Jason Kidd')
entertainment_q4_answer4.question_id= entertainment_q4.id
entertainment_q4_answer4.is_correct= false
entertainment_q4_answer4.save



entertainment_q5 = Question.create(question_string: 'What famous actor lost 45 lbs. to play Andrew Beckett, an AIDS victim fighting wrongful termination?')
entertainment_q5.category_id = Category.where(name: :entertainment).first.id
entertainment_q5.difficulty_level= 1
entertainment_q5.save

entertainment_q5_correct_answer = Answer.create(answer_string: 'Tom Hanks')
entertainment_q5_correct_answer.question_id= entertainment_q5.id
entertainment_q5_correct_answer.is_correct= true
entertainment_q5_correct_answer.save

entertainment_q5_answer2 = Answer.create(answer_string: 'Robin Williams')
entertainment_q5_answer2.question_id= entertainment_q5.id
entertainment_q5_answer2.is_correct= false
entertainment_q5_answer2.save

entertainment_q5_answer3 = Answer.create(answer_string: 'Kevin Spacey')
entertainment_q5_answer3.question_id= entertainment_q5.id
entertainment_q5_answer3.is_correct= false
entertainment_q5_answer3.save

entertainment_q5_answer4 = Answer.create(answer_string: 'William Hurt')
entertainment_q5_answer4.question_id= entertainment_q5.id
entertainment_q5_answer4.is_correct= false
entertainment_q5_answer4.save



entertainment_q6 = Question.create(question_string: 'Who says the famous salute “Live Long and Prosper”?')
entertainment_q6.category_id = Category.where(name: :art).first.id
entertainment_q6.difficulty_level= 1
entertainment_q6.save

entertainment_q6_correct_answer = Answer.create(answer_string: 'Spock')
entertainment_q6_correct_answer.question_id= entertainment_q6.id
entertainment_q6_correct_answer.is_correct= true
entertainment_q6_correct_answer.save

entertainment_q6_answer2 = Answer.create(answer_string: 'Yoda')
entertainment_q6_answer2.question_id= entertainment_q6.id
entertainment_q6_answer2.is_correct= false
entertainment_q6_answer2.save

entertainment_q6_answer3 = Answer.create(answer_string: 'Jim T Kirk')
entertainment_q6_answer3.question_id= entertainment_q6.id
entertainment_q6_answer3.is_correct= false
entertainment_q6_answer3.save

entertainment_q6_answer4 = Answer.create(answer_string: 'Gandalf')
entertainment_q6_answer4.question_id= entertainment_q6.id
entertainment_q6_answer4.is_correct= false
entertainment_q6_answer4.save



entertainment_q7 = Question.create(question_string: 'Who wrote the Goosebump series?')
entertainment_q7.category_id = Category.where(name: :art).first.id
entertainment_q7.difficulty_level= 1
entertainment_q7.save

entertainment_q7_correct_answer = Answer.create(answer_string: 'R.L. Stine')
entertainment_q7_correct_answer.question_id= entertainment_q7.id
entertainment_q7_correct_answer.is_correct= true
entertainment_q7_correct_answer.save

entertainment_q7_answer2 = Answer.create(answer_string: 'Jerry Houser')
entertainment_q7_answer2.question_id= entertainment_q7.id
entertainment_q7_answer2.is_correct= false
entertainment_q7_answer2.save

entertainment_q7_answer3 = Answer.create(answer_string: 'R.J. Connor')
entertainment_q7_answer3.question_id= entertainment_q7.id
entertainment_q7_answer3.is_correct= false
entertainment_q7_answer3.save

entertainment_q7_answer4 = Answer.create(answer_string: 'Thomas Houser')
entertainment_q7_answer4.question_id= entertainment_q7.id
entertainment_q7_answer4.is_correct= false
entertainment_q7_answer4.save



entertainment_q8 = Question.create(question_string: 'Who wrote the phrase “I think, therefore I Exist”?')
entertainment_q8.category_id = Category.where(name: :art).first.id
entertainment_q8.difficulty_level= 1
entertainment_q8.save

entertainment_q8_correct_answer = Answer.create(answer_string: 'Rene Descartes')
entertainment_q8_correct_answer.question_id= entertainment_q8.id
entertainment_q8_correct_answer.is_correct= true
entertainment_q8_correct_answer.save

entertainment_q8_answer2 = Answer.create(answer_string: 'Francis Bacon')
entertainment_q8_answer2.question_id= entertainment_q8.id
entertainment_q8_answer2.is_correct= false
entertainment_q8_answer2.save

entertainment_q8_answer3 = Answer.create(answer_string: 'Socrates')
entertainment_q8_answer3.question_id= entertainment_q8.id
entertainment_q8_answer3.is_correct= false
entertainment_q8_answer3.save

entertainment_q8_answer4 = Answer.create(answer_string: 'Aristotle')
entertainment_q8_answer4.question_id= entertainment_q8.id
entertainment_q8_answer4.is_correct= false
entertainment_q8_answer4.save



entertainment_q9 = Question.create(question_string: "Who wrote Breakfast at Tiffany’s?")
entertainment_q9.category_id = Category.where(name: :art).first.id
entertainment_q9.difficulty_level= 1
entertainment_q9.save

entertainment_q9_correct_answer = Answer.create(answer_string: 'Truman Capote')
entertainment_q9_correct_answer.question_id= entertainment_q9.id
entertainment_q9_correct_answer.is_correct= true
entertainment_q9_correct_answer.save

entertainment_q9_answer2 = Answer.create(answer_string: 'F. Scott Fitzgerald')
entertainment_q9_answer2.question_id= entertainment_q9.id
entertainment_q9_answer2.is_correct= false
entertainment_q9_answer2.save

entertainment_q9_answer3 = Answer.create(answer_string: 'Michael Chabon')
entertainment_q9_answer3.question_id= entertainment_q9.id
entertainment_q9_answer3.is_correct= false
entertainment_q9_answer3.save

entertainment_q9_answer4 = Answer.create(answer_string: 'Virginia Woolf')
entertainment_q9_answer4.question_id= entertainment_q9.id
entertainment_q9_answer4.is_correct= false
entertainment_q9_answer4.save



entertainment_q10 = Question.create(question_string: 'Who wrote the French romantic novel “The Hunchback of Notre Dame”?')
entertainment_q10.category_id = Category.where(name: :art).first.id
entertainment_q10.difficulty_level= 1
entertainment_q10.save

entertainment_q10_correct_answer = Answer.create(answer_string: 'Victor Hugo')
entertainment_q10_correct_answer.question_id= entertainment_q10.id
entertainment_q10_correct_answer.is_correct= true
entertainment_q10_correct_answer.save

entertainment_q10_answer2 = Answer.create(answer_string: 'Pablo Fransisco')
entertainment_q10_answer2.question_id= entertainment_q10.id
entertainment_q10_answer2.is_correct= false
entertainment_q10_answer2.save

entertainment_q10_answer3 = Answer.create(answer_string: 'Jean-Paul Satre')
entertainment_q10_answer3.question_id= entertainment_q10.id
entertainment_q10_answer3.is_correct= false
entertainment_q10_answer3.save

entertainment_q10_answer4 = Answer.create(answer_string: 'Moliere')
entertainment_q10_answer4.question_id= entertainment_q10.id
entertainment_q10_answer4.is_correct= false
entertainment_q10_answer4.save



entertainment_q11 = Question.create(question_string: "Who wrote the cult novel ‘Generation X’?")
entertainment_q11.category_id = Category.where(name: :art).first.id
entertainment_q11.difficulty_level= 1
entertainment_q11.save

entertainment_q11_correct_answer = Answer.create(answer_string: 'Douglas Coupland')
entertainment_q11_correct_answer.question_id= entertainment_q11.id
entertainment_q11_correct_answer.is_correct= true
entertainment_q11_correct_answer.save

entertainment_q11_answer2 = Answer.create(answer_string: 'Jay McInerney')
entertainment_q11_answer2.question_id= entertainment_q11.id
entertainment_q11_answer2.is_correct= false
entertainment_q11_answer2.save

entertainment_q11_answer3 = Answer.create(answer_string: 'Bret Easton Ellis')
entertainment_q11_answer3.question_id= entertainment_q11.id
entertainment_q11_answer3.is_correct= false
entertainment_q11_answer3.save

entertainment_q11_answer4 = Answer.create(answer_string: 'Will Self')
entertainment_q11_answer4.question_id= entertainment_q11.id
entertainment_q11_answer4.is_correct= false
entertainment_q11_answer4.save



entertainment_q12 = Question.create(question_string: "Who went on a ‘Blonde Ambition’ tour in 1990?")
entertainment_q12.category_id = Category.where(name: :art).first.id
entertainment_q12.difficulty_level= 1
entertainment_q12.save

entertainment_q12_correct_answer = Answer.create(answer_string: 'Blondie')
entertainment_q12_correct_answer.question_id= entertainment_q12.id
entertainment_q12_correct_answer.is_correct= true
entertainment_q12_correct_answer.save

entertainment_q12_answer2 = Answer.create(answer_string: 'Madonna')
entertainment_q12_answer2.question_id= entertainment_q12.id
entertainment_q12_answer2.is_correct= false
entertainment_q12_answer2.save

entertainment_q12_answer3 = Answer.create(answer_string: 'Annie Lennoz')
entertainment_q12_answer3.question_id= entertainment_q12.id
entertainment_q12_answer3.is_correct= false
entertainment_q12_answer3.save

entertainment_q12_answer4 = Answer.create(answer_string: 'Cher')
entertainment_q12_answer4.question_id= entertainment_q12.id
entertainment_q12_answer4.is_correct= false
entertainment_q12_answer4.save
# </editor-fold>

# <editor-fold desc="Geography Questions">
geography_q1 = Question.create(question_string: "The Grand Canyon was carved out of solid rock by the cutting action of what river?")
geography_q1.category_id = Category.where(name: :geography).first.id
geography_q1.difficulty_level= 1
geography_q1.save

geography_q1_correct_answer = Answer.create(answer_string: "Colorado River")
geography_q1_correct_answer.question_id= geography_q1.id
geography_q1_correct_answer.is_correct= true
geography_q1_correct_answer.save

geography_q1_answer2 = Answer.create(answer_string: "Grand Canyon River")
geography_q1_answer2.question_id= geography_q1.id
geography_q1_answer2.is_correct= false
geography_q1_answer2.save

geography_q1_answer3 = Answer.create(answer_string: 'Little Rock River')
geography_q1_answer3.question_id= geography_q1.id
geography_q1_answer3.is_correct= false
geography_q1_answer3.save

geography_q1_answer4 = Answer.create(answer_string: 'Thames River')
geography_q1_answer4.question_id= geography_q1.id
geography_q1_answer4.is_correct= false
geography_q1_answer4.save


geography_q2 = Question.create(question_string: "What is Earth's largest continent?")
geography_q2.category_id = Category.where(name: :geography).first.id
geography_q2.difficulty_level= 1
geography_q2.save

geography_q2_correct_answer = Answer.create(answer_string: 'Asia')
geography_q2_correct_answer.question_id= geography_q2.id
geography_q2_correct_answer.is_correct= true
geography_q2_correct_answer.save

geography_q2_answer2 = Answer.create(answer_string: 'Africa')
geography_q2_answer2.question_id= geography_q2.id
geography_q2_answer2.is_correct= false
geography_q2_answer2.save

geography_q2_answer3 = Answer.create(answer_string: 'Europe')
geography_q2_answer3.question_id= geography_q2.id
geography_q2_answer3.is_correct= false
geography_q2_answer3.save

geography_q2_answer4 = Answer.create(answer_string: 'Antarctica')
geography_q2_answer4.question_id= geography_q2.id
geography_q2_answer4.is_correct= false
geography_q2_answer4.save


geography_q3 = Question.create(question_string: 'What razor-thin country accounts for more than half of the western coastline of South America?')
geography_q3.category_id = Category.where(name: :geography).first.id
geography_q3.difficulty_level= 1
geography_q3.save

geography_q3_correct_answer = Answer.create(answer_string: 'Chile')
geography_q3_correct_answer.question_id= geography_q3.id
geography_q3_correct_answer.is_correct= true
geography_q3_correct_answer.save

geography_q3_answer2 = Answer.create(answer_string: 'Ecuador')
geography_q3_answer2.question_id= geography_q3.id
geography_q3_answer2.is_correct= false
geography_q3_answer2.save

geography_q3_answer3 = Answer.create(answer_string: 'Peru')
geography_q3_answer3.question_id= geography_q3.id
geography_q3_answer3.is_correct= false
geography_q3_answer3.save

geography_q3_answer4 = Answer.create(answer_string: 'Bolivia')
geography_q3_answer4.question_id= geography_q3.id
geography_q3_answer4.is_correct= false
geography_q3_answer4.save


geography_q4 = Question.create(question_string: 'What river runs through Baghdad?')
geography_q4.category_id = Category.where(name: :geography).first.id
geography_q4.difficulty_level= 1
geography_q4.save

geography_q4_correct_answer = Answer.create(answer_string: 'Tigris')
geography_q4_correct_answer.question_id= geography_q4.id
geography_q4_correct_answer.is_correct= true
geography_q4_correct_answer.save

geography_q4_answer2 = Answer.create(answer_string: 'Euphrates')
geography_q4_answer2.question_id= geography_q4.id
geography_q4_answer2.is_correct= false
geography_q4_answer2.save

geography_q4_answer3 = Answer.create(answer_string: 'Karun')
geography_q4_answer3.question_id= geography_q4.id
geography_q4_answer3.is_correct= false
geography_q4_answer3.save

geography_q4_answer4 = Answer.create(answer_string: 'Jordan')
geography_q4_answer4.question_id= geography_q4.id
geography_q4_answer4.is_correct= false
geography_q4_answer4.save


geography_q5 = Question.create(question_string: 'What country is home to Kangaroo Island?')
geography_q5.category_id = Category.where(name: :geography).first.id
geography_q5.difficulty_level= 1
geography_q5.save

geography_q5_correct_answer = Answer.create(answer_string: 'Australia')
geography_q5_correct_answer.question_id= geography_q5.id
geography_q5_correct_answer.is_correct= true
geography_q5_correct_answer.save

geography_q5_answer2 = Answer.create(answer_string: 'France')
geography_q5_answer2.question_id= geography_q5.id
geography_q5_answer2.is_correct= false
geography_q5_answer2.save

geography_q5_answer3 = Answer.create(answer_string: 'Japan')
geography_q5_answer3.question_id= geography_q5.id
geography_q5_answer3.is_correct= false
geography_q5_answer3.save

geography_q5_answer4 = Answer.create(answer_string: 'Great Britain')
geography_q5_answer4.question_id= geography_q5.id
geography_q5_answer4.is_correct= false
geography_q5_answer4.save



geography_q6 = Question.create(question_string: 'La Española is the most populated island in America. To how many countries does it belong?')
geography_q6.category_id = Category.where(name: :art).first.id
geography_q6.difficulty_level= 1
geography_q6.save

geography_q6_correct_answer = Answer.create(answer_string: 'One')
geography_q6_correct_answer.question_id= geography_q6.id
geography_q6_correct_answer.is_correct= true
geography_q6_correct_answer.save

geography_q6_answer2 = Answer.create(answer_string: 'Two')
geography_q6_answer2.question_id= geography_q6.id
geography_q6_answer2.is_correct= false
geography_q6_answer2.save

geography_q6_answer3 = Answer.create(answer_string: 'Three')
geography_q6_answer3.question_id= geography_q6.id
geography_q6_answer3.is_correct= false
geography_q6_answer3.save

geography_q6_answer4 = Answer.create(answer_string: 'Four')
geography_q6_answer4.question_id= geography_q6.id
geography_q6_answer4.is_correct= false
geography_q6_answer4.save



geography_q7 = Question.create(question_string: 'Which continent touches each line of longitude?')
geography_q7.category_id = Category.where(name: :art).first.id
geography_q7.difficulty_level= 1
geography_q7.save

geography_q7_correct_answer = Answer.create(answer_string: 'Antarctica')
geography_q7_correct_answer.question_id= geography_q7.id
geography_q7_correct_answer.is_correct= true
geography_q7_correct_answer.save

geography_q7_answer2 = Answer.create(answer_string: 'North America')
geography_q7_answer2.question_id= geography_q7.id
geography_q7_answer2.is_correct= false
geography_q7_answer2.save

geography_q7_answer3 = Answer.create(answer_string: 'Asia')
geography_q7_answer3.question_id= geography_q7.id
geography_q7_answer3.is_correct= false
geography_q7_answer3.save

geography_q7_answer4 = Answer.create(answer_string: 'Australia')
geography_q7_answer4.question_id= geography_q7.id
geography_q7_answer4.is_correct= false
geography_q7_answer4.save



geography_q8 = Question.create(question_string: 'What country’s national flag provided the symbol for the Red Cross?')
geography_q8.category_id = Category.where(name: :art).first.id
geography_q8.difficulty_level= 1
geography_q8.save

geography_q8_correct_answer = Answer.create(answer_string: 'Switzerland')
geography_q8_correct_answer.question_id= geography_q8.id
geography_q8_correct_answer.is_correct= true
geography_q8_correct_answer.save

geography_q8_answer2 = Answer.create(answer_string: 'France')
geography_q8_answer2.question_id= geography_q8.id
geography_q8_answer2.is_correct= false
geography_q8_answer2.save

geography_q8_answer3 = Answer.create(answer_string: 'Belgium')
geography_q8_answer3.question_id= geography_q8.id
geography_q8_answer3.is_correct= false
geography_q8_answer3.save

geography_q8_answer4 = Answer.create(answer_string: 'Italy')
geography_q8_answer4.question_id= geography_q8.id
geography_q8_answer4.is_correct= false
geography_q8_answer4.save



geography_q9 = Question.create(question_string: 'Which US state produces the most potatoes?')
geography_q9.category_id = Category.where(name: :art).first.id
geography_q9.difficulty_level= 1
geography_q9.save

geography_q9_correct_answer = Answer.create(answer_string: 'Idaho')
geography_q9_correct_answer.question_id= geography_q9.id
geography_q9_correct_answer.is_correct= true
geography_q9_correct_answer.save

geography_q9_answer2 = Answer.create(answer_string: 'California')
geography_q9_answer2.question_id= geography_q9.id
geography_q9_answer2.is_correct= false
geography_q9_answer2.save

geography_q9_answer3 = Answer.create(answer_string: 'Montana')
geography_q9_answer3.question_id= geography_q9.id
geography_q9_answer3.is_correct= false
geography_q9_answer3.save

geography_q9_answer4 = Answer.create(answer_string: 'Wyoming')
geography_q9_answer4.question_id= geography_q9.id
geography_q9_answer4.is_correct= false
geography_q9_answer4.save



geography_q10 = Question.create(question_string: 'Which is the smallest continent?')
geography_q10.category_id = Category.where(name: :art).first.id
geography_q10.difficulty_level= 1
geography_q10.save

geography_q10_correct_answer = Answer.create(answer_string: 'Australia')
geography_q10_correct_answer.question_id= geography_q10.id
geography_q10_correct_answer.is_correct= true
geography_q10_correct_answer.save

geography_q10_answer2 = Answer.create(answer_string: 'They are all the same size')
geography_q10_answer2.question_id= geography_q10.id
geography_q10_answer2.is_correct= false
geography_q10_answer2.save

geography_q10_answer3 = Answer.create(answer_string: 'North America')
geography_q10_answer3.question_id= geography_q10.id
geography_q10_answer3.is_correct= false
geography_q10_answer3.save

geography_q10_answer4 = Answer.create(answer_string: 'South America')
geography_q10_answer4.question_id= geography_q10.id
geography_q10_answer4.is_correct= false
geography_q10_answer4.save



geography_q11 = Question.create(question_string: 'Which country would you most associate the tree Eucalyptus with?')
geography_q11.category_id = Category.where(name: :art).first.id
geography_q11.difficulty_level= 1
geography_q11.save

geography_q11_correct_answer = Answer.create(answer_string: 'Australia')
geography_q11_correct_answer.question_id= geography_q11.id
geography_q11_correct_answer.is_correct= true
geography_q11_correct_answer.save

geography_q11_answer2 = Answer.create(answer_string: 'Nepal')
geography_q11_answer2.question_id= geography_q11.id
geography_q11_answer2.is_correct= false
geography_q11_answer2.save

geography_q11_answer3 = Answer.create(answer_string: 'Germany')
geography_q11_answer3.question_id= geography_q11.id
geography_q11_answer3.is_correct= false
geography_q11_answer3.save

geography_q11_answer4 = Answer.create(answer_string: 'Brazil')
geography_q11_answer4.question_id= geography_q11.id
geography_q11_answer4.is_correct= false
geography_q11_answer4.save



geography_q12 = Question.create(question_string: 'What is the state flower of the Hawaiian islands?')
geography_q12.category_id = Category.where(name: :art).first.id
geography_q12.difficulty_level= 1
geography_q12.save

geography_q12_correct_answer = Answer.create(answer_string: 'Hibiscus')
geography_q12_correct_answer.question_id= geography_q12.id
geography_q12_correct_answer.is_correct= true
geography_q12_correct_answer.save

geography_q12_answer2 = Answer.create(answer_string: 'Sunflower')
geography_q12_answer2.question_id= geography_q12.id
geography_q12_answer2.is_correct= false
geography_q12_answer2.save

geography_q12_answer3 = Answer.create(answer_string: 'Plumeria')
geography_q12_answer3.question_id= geography_q12.id
geography_q12_answer3.is_correct= false
geography_q12_answer3.save

geography_q12_answer4 = Answer.create(answer_string: 'Coconut Flower')
geography_q12_answer4.question_id= geography_q12.id
geography_q12_answer4.is_correct= false
geography_q12_answer4.save
# </editor-fold>

# <editor-fold desc="History Questions">
history_q1 = Question.create(question_string: "What was the first state to enter the union after the original thirteen?")
history_q1.category_id = Category.where(name: :history).first.id
history_q1.difficulty_level= 1
history_q1.save

history_q1_correct_answer = Answer.create(answer_string: "Vermont")
history_q1_correct_answer.question_id= history_q1.id
history_q1_correct_answer.is_correct= true
history_q1_correct_answer.save

history_q1_answer2 = Answer.create(answer_string: "Alabama")
history_q1_answer2.question_id= history_q1.id
history_q1_answer2.is_correct= false
history_q1_answer2.save

history_q1_answer3 = Answer.create(answer_string: 'Texas')
history_q1_answer3.question_id= history_q1.id
history_q1_answer3.is_correct= false
history_q1_answer3.save

history_q1_answer4 = Answer.create(answer_string: 'California')
history_q1_answer4.question_id= history_q1.id
history_q1_answer4.is_correct= false
history_q1_answer4.save


history_q2 = Question.create(question_string: 'What is the earliest surviving system of laws?')
history_q2.category_id = Category.where(name: :history).first.id
history_q2.difficulty_level= 1
history_q2.save

history_q2_correct_answer = Answer.create(answer_string: 'Code of Hammurabi')
history_q2_correct_answer.question_id= history_q2.id
history_q2_correct_answer.is_correct= true
history_q2_correct_answer.save

history_q2_answer2 = Answer.create(answer_string: 'Rosetta Stone')
history_q2_answer2.question_id= history_q2.id
history_q2_answer2.is_correct= false
history_q2_answer2.save

history_q2_answer3 = Answer.create(answer_string: 'Hebrew Torah')
history_q2_answer3.question_id= history_q2.id
history_q2_answer3.is_correct= false
history_q2_answer3.save

history_q2_answer4 = Answer.create(answer_string: 'Shabaka Stone')
history_q2_answer4.question_id= history_q2.id
history_q2_answer4.is_correct= false
history_q2_answer4.save


history_q3 = Question.create(question_string: 'What type of gun did John Wilkes Booth use to assassinate U.S. President Abraham Lincoln?')
history_q3.category_id = Category.where(name: :history).first.id
history_q3.difficulty_level= 1
history_q3.save

history_q3_correct_answer = Answer.create(answer_string: 'Derringer Pistol')
history_q3_correct_answer.question_id= history_q3.id
history_q3_correct_answer.is_correct= true
history_q3_correct_answer.save

history_q3_answer2 = Answer.create(answer_string: 'Colt Peacemaker')
history_q3_answer2.question_id= history_q3.id
history_q3_answer2.is_correct= false
history_q3_answer2.save

history_q3_answer3 = Answer.create(answer_string: 'Smith & Wesson')
history_q3_answer3.question_id= history_q3.id
history_q3_answer3.is_correct= false
history_q3_answer3.save

history_q3_answer4 = Answer.create(answer_string: 'Winchester Rifle')
history_q3_answer4.question_id= history_q3.id
history_q3_answer4.is_correct= false
history_q3_answer4.save


history_q4 = Question.create(question_string: 'What was the last battle of the Napoleonic Wars?')
history_q4.category_id = Category.where(name: :history).first.id
history_q4.difficulty_level= 1
history_q4.save

history_q4_correct_answer = Answer.create(answer_string: 'Battle of Wavre')
history_q4_correct_answer.question_id= history_q4.id
history_q4_correct_answer.is_correct= true
history_q4_correct_answer.save

history_q4_answer2 = Answer.create(answer_string: 'Battle of Trafalgar')
history_q4_answer2.question_id= history_q4.id
history_q4_answer2.is_correct= false
history_q4_answer2.save

history_q4_answer3 = Answer.create(answer_string: 'Battle of the Nile')
history_q4_answer3.question_id= history_q4.id
history_q4_answer3.is_correct= false
history_q4_answer3.save

history_q4_answer4 = Answer.create(answer_string: 'Battle of Waterloo')
history_q4_answer4.question_id= history_q4.id
history_q4_answer4.is_correct= false
history_q4_answer4.save


history_q5 = Question.create(question_string: "The world's first postage stamp was introduced in what year?")
history_q5.category_id = Category.where(name: :history).first.id
history_q5.difficulty_level= 1
history_q5.save

history_q5_correct_answer = Answer.create(answer_string: '1840')
history_q5_correct_answer.question_id= history_q5.id
history_q5_correct_answer.is_correct= true
history_q5_correct_answer.save

history_q5_answer2 = Answer.create(answer_string: '1690')
history_q5_answer2.question_id= history_q5.id
history_q5_answer2.is_correct= false
history_q5_answer2.save

history_q5_answer3 = Answer.create(answer_string: '1760')
history_q5_answer3.question_id= history_q5.id
history_q5_answer3.is_correct= false
history_q5_answer3.save

history_q5_answer4 = Answer.create(answer_string: '1910')
history_q5_answer4.question_id= history_q5.id
history_q5_answer4.is_correct= false
history_q5_answer4.save



history_q6 = Question.create(question_string: "With what animal are the Jewish represented in the graphic novel about the Holocaust ‘Maus’?")
history_q6.category_id = Category.where(name: :art).first.id
history_q6.difficulty_level= 1
history_q6.save

history_q6_correct_answer = Answer.create(answer_string: 'Mice')
history_q6_correct_answer.question_id= history_q6.id
history_q6_correct_answer.is_correct= true
history_q6_correct_answer.save

history_q6_answer2 = Answer.create(answer_string: 'Cats')
history_q6_answer2.question_id= history_q6.id
history_q6_answer2.is_correct= false
history_q6_answer2.save

history_q6_answer3 = Answer.create(answer_string: 'Pigs')
history_q6_answer3.question_id= history_q6.id
history_q6_answer3.is_correct= false
history_q6_answer3.save

history_q6_answer4 = Answer.create(answer_string: 'Dogs')
history_q6_answer4.question_id= history_q6.id
history_q6_answer4.is_correct= false
history_q6_answer4.save



history_q7 = Question.create(question_string: 'Who was assassinated in Sarajevo, sparking World War I?')
history_q7.category_id = Category.where(name: :art).first.id
history_q7.difficulty_level= 1
history_q7.save

history_q7_correct_answer = Answer.create(answer_string: 'Archduke Francis Ferdinand and His Wife')
history_q7_correct_answer.question_id= history_q7.id
history_q7_correct_answer.is_correct= true
history_q7_correct_answer.save

history_q7_answer2 = Answer.create(answer_string: 'Paul Von Hindenburg')
history_q7_answer2.question_id= history_q7.id
history_q7_answer2.is_correct= false
history_q7_answer2.save

history_q7_answer3 = Answer.create(answer_string: 'Kaiser Wilhelm the Second')
history_q7_answer3.question_id= history_q7.id
history_q7_answer3.is_correct= false
history_q7_answer3.save

history_q7_answer4 = Answer.create(answer_string: 'Tsar Nicholas')
history_q7_answer4.question_id= history_q7.id
history_q7_answer4.is_correct= false
history_q7_answer4.save



history_q8 = Question.create(question_string: "Who’s the current president of Guatemala?")
history_q8.category_id = Category.where(name: :art).first.id
history_q8.difficulty_level= 1
history_q8.save

history_q8_correct_answer = Answer.create(answer_string: "Otto Pérez Molina")
history_q8_correct_answer.question_id= history_q8.id
history_q8_correct_answer.is_correct= true
history_q8_correct_answer.save

history_q8_answer2 = Answer.create(answer_string: 'Daniel Ortega')
history_q8_answer2.question_id= history_q8.id
history_q8_answer2.is_correct= false
history_q8_answer2.save

history_q8_answer3 = Answer.create(answer_string: 'Porfirio Lobo')
history_q8_answer3.question_id= history_q8.id
history_q8_answer3.is_correct= false
history_q8_answer3.save

history_q8_answer4 = Answer.create(answer_string: 'Ricardo Martinelli')
history_q8_answer4.question_id= history_q8.id
history_q8_answer4.is_correct= false
history_q8_answer4.save



history_q9 = Question.create(question_string: "Who was D’Artagnan?")
history_q9.category_id = Category.where(name: :art).first.id
history_q9.difficulty_level= 1
history_q9.save

history_q9_correct_answer = Answer.create(answer_string: 'A French Musketeer')
history_q9_correct_answer.question_id= history_q9.id
history_q9_correct_answer.is_correct= true
history_q9_correct_answer.save

history_q9_answer2 = Answer.create(answer_string: 'A French King')
history_q9_answer2.question_id= history_q9.id
history_q9_answer2.is_correct= false
history_q9_answer2.save

history_q9_answer3 = Answer.create(answer_string: 'A French Warrior')
history_q9_answer3.question_id= history_q9.id
history_q9_answer3.is_correct= false
history_q9_answer3.save

history_q9_answer4 = Answer.create(answer_string: 'Son of a French King')
history_q9_answer4.question_id= history_q9.id
history_q9_answer4.is_correct= false
history_q9_answer4.save



history_q10 = Question.create(question_string: 'What were the Spartans of Ancient Greece most known for?')
history_q10.category_id = Category.where(name: :art).first.id
history_q10.difficulty_level= 1
history_q10.save

history_q10_correct_answer = Answer.create(answer_string: 'Military')
history_q10_correct_answer.question_id= history_q10.id
history_q10_correct_answer.is_correct= true
history_q10_correct_answer.save

history_q10_answer2 = Answer.create(answer_string: 'Culture')
history_q10_answer2.question_id= history_q10.id
history_q10_answer2.is_correct= false
history_q10_answer2.save

history_q10_answer3 = Answer.create(answer_string: 'Science')
history_q10_answer3.question_id= history_q10.id
history_q10_answer3.is_correct= false
history_q10_answer3.save

history_q10_answer4 = Answer.create(answer_string: 'Architecure')
history_q10_answer4.question_id= history_q10.id
history_q10_answer4.is_correct= false
history_q10_answer4.save



history_q11 = Question.create(question_string: 'Who is on the hundred dollar bill?')
history_q11.category_id = Category.where(name: :art).first.id
history_q11.difficulty_level= 1
history_q11.save

history_q11_correct_answer = Answer.create(answer_string: 'Benjamin Franklin')
history_q11_correct_answer.question_id= history_q11.id
history_q11_correct_answer.is_correct= true
history_q11_correct_answer.save

history_q11_answer2 = Answer.create(answer_string: 'Thomas Jefferson')
history_q11_answer2.question_id= history_q11.id
history_q11_answer2.is_correct= false
history_q11_answer2.save

history_q11_answer3 = Answer.create(answer_string: 'George Washington')
history_q11_answer3.question_id= history_q11.id
history_q11_answer3.is_correct= false
history_q11_answer3.save

history_q11_answer4 = Answer.create(answer_string: 'Robert E Lee')
history_q11_answer4.question_id= history_q11.id
history_q11_answer4.is_correct= false
history_q11_answer4.save



history_q12 = Question.create(question_string: 'What inspired Walt Disney to make Disney?')
history_q12.category_id = Category.where(name: :art).first.id
history_q12.difficulty_level= 1
history_q12.save

history_q12_correct_answer = Answer.create(answer_string: 'A Mouse')
history_q12_correct_answer.question_id= history_q12.id
history_q12_correct_answer.is_correct= true
history_q12_correct_answer.save

history_q12_answer2 = Answer.create(answer_string: 'A Bird')
history_q12_answer2.question_id= history_q12.id
history_q12_answer2.is_correct= false
history_q12_answer2.save

history_q12_answer3 = Answer.create(answer_string: 'A Dog')
history_q12_answer3.question_id= history_q12.id
history_q12_answer3.is_correct= false
history_q12_answer3.save

history_q12_answer4 = Answer.create(answer_string: 'None of the Above')
history_q12_answer4.question_id= history_q12.id
history_q12_answer4.is_correct= false
history_q12_answer4.save
# </editor-fold>

# <editor-fold desc="Science Questions">
science_q1 = Question.create(question_string: "The air we breathe is composed 99% of what two chemicals?")
science_q1.category_id = Category.where(name: :science).first.id
science_q1.difficulty_level= 1
science_q1.save

science_q1_correct_answer = Answer.create(answer_string: "78% nitrogen, 21.5% oxygen")
science_q1_correct_answer.question_id= science_q1.id
science_q1_correct_answer.is_correct= true
science_q1_correct_answer.save

science_q1_answer2 = Answer.create(answer_string: "55% oxygne, 45% helium")
science_q1_answer2.question_id= science_q1.id
science_q1_answer2.is_correct= false
science_q1_answer2.save

science_q1_answer3 = Answer.create(answer_string: '100% water')
science_q1_answer3.question_id= science_q1.id
science_q1_answer3.is_correct= false
science_q1_answer3.save

science_q1_answer4 = Answer.create(answer_string: 'Bananas')
science_q1_answer4.question_id= science_q1.id
science_q1_answer4.is_correct= false
science_q1_answer4.save


science_q2 = Question.create(question_string: 'When was the idea of the atom first introduced?')
science_q2.category_id = Category.where(name: :science).first.id
science_q2.difficulty_level= 1
science_q2.save

science_q2_correct_answer = Answer.create(answer_string: '450 B.C.')
science_q2_correct_answer.question_id= science_q2.id
science_q2_correct_answer.is_correct= true
science_q2_correct_answer.save

science_q2_answer2 = Answer.create(answer_string: '1050')
science_q2_answer2.question_id= science_q2.id
science_q2_answer2.is_correct= false
science_q2_answer2.save

science_q2_answer3 = Answer.create(answer_string: '1791')
science_q2_answer3.question_id= science_q2.id
science_q2_answer3.is_correct= false
science_q2_answer3.save

science_q2_answer4 = Answer.create(answer_string: '1942')
science_q2_answer4.question_id= science_q2.id
science_q2_answer4.is_correct= false
science_q2_answer4.save


science_q3 = Question.create(question_string: 'In 2004, what was discovered on the island of Flores in Indonesia?')
science_q3.category_id = Category.where(name: :science).first.id
science_q3.difficulty_level= 1
science_q3.save

science_q3_correct_answer = Answer.create(answer_string: 'Remains of a hobbit-sized human species')
science_q3_correct_answer.question_id= science_q3.id
science_q3_correct_answer.is_correct= true
science_q3_correct_answer.save

science_q3_answer2 = Answer.create(answer_string: 'A living dinosaur')
science_q3_answer2.question_id= science_q3.id
science_q3_answer2.is_correct= false
science_q3_answer2.save

science_q3_answer3 = Answer.create(answer_string: 'A tribe whose members commonly live over 150 years')
science_q3_answer3.question_id= science_q3.id
science_q3_answer3.is_correct= false
science_q3_answer3.save

science_q3_answer4 = Answer.create(answer_string: 'A plant with a mammal-like brain')
science_q3_answer4.question_id= science_q3.id
science_q3_answer4.is_correct= false
science_q3_answer4.save


science_q4 = Question.create(question_string: 'What is the gestation period of an Hippopotamus?')
science_q4.category_id = Category.where(name: :science).first.id
science_q4.difficulty_level= 1
science_q4.save

science_q4_correct_answer = Answer.create(answer_string: '8 Months')
science_q4_correct_answer.question_id= science_q4.id
science_q4_correct_answer.is_correct= true
science_q4_correct_answer.save

science_q4_answer2 = Answer.create(answer_string: '4 Months')
science_q4_answer2.question_id= science_q4.id
science_q4_answer2.is_correct= false
science_q4_answer2.save

science_q4_answer3 = Answer.create(answer_string: '12 Months')
science_q4_answer3.question_id= science_q4.id
science_q4_answer3.is_correct= false
science_q4_answer3.save

science_q4_answer4 = Answer.create(answer_string: '16 Months')
science_q4_answer4.question_id= science_q4.id
science_q4_answer4.is_correct= false
science_q4_answer4.save


science_q5 = Question.create(question_string: 'In what type of matter are atoms most tightly packed?')
science_q5.category_id = Category.where(name: :science).first.id
science_q5.difficulty_level= 1
science_q5.save

science_q5_correct_answer = Answer.create(answer_string: 'Solids')
science_q5_correct_answer.question_id= science_q5.id
science_q5_correct_answer.is_correct= true
science_q5_correct_answer.save

science_q5_answer2 = Answer.create(answer_string: 'Liquids')
science_q5_answer2.question_id= science_q5.id
science_q5_answer2.is_correct= false
science_q5_answer2.save

science_q5_answer3 = Answer.create(answer_string: 'Gases')
science_q5_answer3.question_id= science_q5.id
science_q5_answer3.is_correct= false
science_q5_answer3.save

science_q5_answer4 = Answer.create(answer_string: 'All are the same')
science_q5_answer4.question_id= science_q5.id
science_q5_answer4.is_correct= false
science_q5_answer4.save



science_q6 = Question.create(question_string: 'What is the center of an atom called?')
science_q6.category_id = Category.where(name: :art).first.id
science_q6.difficulty_level= 1
science_q6.save

science_q6_correct_answer = Answer.create(answer_string: 'Nucleus')
science_q6_correct_answer.question_id= science_q6.id
science_q6_correct_answer.is_correct= true
science_q6_correct_answer.save

science_q6_answer2 = Answer.create(answer_string: 'DNA')
science_q6_answer2.question_id= science_q6.id
science_q6_answer2.is_correct= false
science_q6_answer2.save

science_q6_answer3 = Answer.create(answer_string: 'Molecule')
science_q6_answer3.question_id= science_q6.id
science_q6_answer3.is_correct= false
science_q6_answer3.save

science_q6_answer4 = Answer.create(answer_string: 'Cell')
science_q6_answer4.question_id= science_q6.id
science_q6_answer4.is_correct= false
science_q6_answer4.save



science_q7 = Question.create(question_string: 'What is the biggest source of Vitamin D?')
science_q7.category_id = Category.where(name: :art).first.id
science_q7.difficulty_level= 1
science_q7.save

science_q7_correct_answer = Answer.create(answer_string: 'Sun')
science_q7_correct_answer.question_id= science_q7.id
science_q7_correct_answer.is_correct= true
science_q7_correct_answer.save

science_q7_answer2 = Answer.create(answer_string: 'Eggs')
science_q7_answer2.question_id= science_q7.id
science_q7_answer2.is_correct= false
science_q7_answer2.save

science_q7_answer3 = Answer.create(answer_string: 'Milk')
science_q7_answer3.question_id= science_q7.id
science_q7_answer3.is_correct= false
science_q7_answer3.save

science_q7_answer4 = Answer.create(answer_string: 'Fish')
science_q7_answer4.question_id= science_q7.id
science_q7_answer4.is_correct= false
science_q7_answer4.save



science_q8 = Question.create(question_string: 'What is the name of the scientist that discovered Uranus?')
science_q8.category_id = Category.where(name: :art).first.id
science_q8.difficulty_level= 1
science_q8.save

science_q8_correct_answer = Answer.create(answer_string: 'William Hershcel')
science_q8_correct_answer.question_id= science_q8.id
science_q8_correct_answer.is_correct= true
science_q8_correct_answer.save

science_q8_answer2 = Answer.create(answer_string: 'Galileo Galilei')
science_q8_answer2.question_id= science_q8.id
science_q8_answer2.is_correct= false
science_q8_answer2.save

science_q8_answer3 = Answer.create(answer_string: 'Isaac Newton')
science_q8_answer3.question_id= science_q8.id
science_q8_answer3.is_correct= false
science_q8_answer3.save

science_q8_answer4 = Answer.create(answer_string: 'Herschel Krustofsky')
science_q8_answer4.question_id= science_q8.id
science_q8_answer4.is_correct= false
science_q8_answer4.save



science_q9 = Question.create(question_string: 'What is the unit of measure for sound?')
science_q9.category_id = Category.where(name: :art).first.id
science_q9.difficulty_level= 1
science_q9.save

science_q9_correct_answer = Answer.create(answer_string: 'Decibels')
science_q9_correct_answer.question_id= science_q9.id
science_q9_correct_answer.is_correct= true
science_q9_correct_answer.save

science_q9_answer2 = Answer.create(answer_string: 'Photons')
science_q9_answer2.question_id= science_q9.id
science_q9_answer2.is_correct= false
science_q9_answer2.save

science_q9_answer3 = Answer.create(answer_string: 'Audibles')
science_q9_answer3.question_id= science_q9.id
science_q9_answer3.is_correct= false
science_q9_answer3.save

science_q9_answer4 = Answer.create(answer_string: 'Electrons')
science_q9_answer4.question_id= science_q9.id
science_q9_answer4.is_correct= false
science_q9_answer4.save



science_q10 = Question.create(question_string: 'How do we call a persistent obsession with health?')
science_q10.category_id = Category.where(name: :art).first.id
science_q10.difficulty_level= 1
science_q10.save

science_q10_correct_answer = Answer.create(answer_string: 'Hypochondria')
science_q10_correct_answer.question_id= science_q10.id
science_q10_correct_answer.is_correct= true
science_q10_correct_answer.save

science_q10_answer2 = Answer.create(answer_string: 'Anxiety')
science_q10_answer2.question_id= science_q10.id
science_q10_answer2.is_correct= false
science_q10_answer2.save

science_q10_answer3 = Answer.create(answer_string: 'Sickness Phobie')
science_q10_answer3.question_id= science_q10.id
science_q10_answer3.is_correct= false
science_q10_answer3.save

science_q10_answer4 = Answer.create(answer_string: 'Hyperchondria')
science_q10_answer4.question_id= science_q10.id
science_q10_answer4.is_correct= false
science_q10_answer4.save



science_q11 = Question.create(question_string: 'What does cryptozoology study?')
science_q11.category_id = Category.where(name: :art).first.id
science_q11.difficulty_level= 1
science_q11.save

science_q11_correct_answer = Answer.create(answer_string: 'Animals whose existence is improbable')
science_q11_correct_answer.question_id= science_q11.id
science_q11_correct_answer.is_correct= true
science_q11_correct_answer.save

science_q11_answer2 = Answer.create(answer_string: 'Animals who live in crypts')
science_q11_answer2.question_id= science_q11.id
science_q11_answer2.is_correct= false
science_q11_answer2.save

science_q11_answer3 = Answer.create(answer_string: 'Endangered Animals')
science_q11_answer3.question_id= science_q11.id
science_q11_answer3.is_correct= false
science_q11_answer3.save

science_q11_answer4 = Answer.create(answer_string: 'None of these')
science_q11_answer4.question_id= science_q11.id
science_q11_answer4.is_correct= false
science_q11_answer4.save



science_q12 = Question.create(question_string: 'Which element is Ra in the periodic table?')
science_q12.category_id = Category.where(name: :art).first.id
science_q12.difficulty_level= 1
science_q12.save

science_q12_correct_answer = Answer.create(answer_string: 'Radium')
science_q12_correct_answer.question_id= science_q12.id
science_q12_correct_answer.is_correct= true
science_q12_correct_answer.save

science_q12_answer2 = Answer.create(answer_string: 'Rubidium')
science_q12_answer2.question_id= science_q12.id
science_q12_answer2.is_correct= false
science_q12_answer2.save

science_q12_answer3 = Answer.create(answer_string: 'Radio')
science_q12_answer3.question_id= science_q12.id
science_q12_answer3.is_correct= false
science_q12_answer3.save

science_q12_answer4 = Answer.create(answer_string: 'Rhodium')
science_q12_answer4.question_id= science_q12.id
science_q12_answer4.is_correct= false
science_q12_answer4.save
# </editor-fold>

# <editor-fold desc="Sports Questions">
sports_q1 = Question.create(question_string: "How long is a tackle football field, including the end zones? ")
sports_q1.category_id = Category.where(name: :sports).first.id
sports_q1.difficulty_level= 1
sports_q1.save

sports_q1_correct_answer = Answer.create(answer_string: "120 yards")
sports_q1_correct_answer.question_id= sports_q1.id
sports_q1_correct_answer.is_correct= true
sports_q1_correct_answer.save

sports_q1_answer2 = Answer.create(answer_string: "100 yards")
sports_q1_answer2.question_id= sports_q1.id
sports_q1_answer2.is_correct= false
sports_q1_answer2.save

sports_q1_answer3 = Answer.create(answer_string: '110 yards')
sports_q1_answer3.question_id= sports_q1.id
sports_q1_answer3.is_correct= false
sports_q1_answer3.save

sports_q1_answer4 = Answer.create(answer_string: '105 yards')
sports_q1_answer4.question_id= sports_q1.id
sports_q1_answer4.is_correct= false
sports_q1_answer4.save


sports_q2 = Question.create(question_string: 'What boxer holds the record for youngest professional debut?')
sports_q2.category_id = Category.where(name: :sports).first.id
sports_q2.difficulty_level= 1
sports_q2.save

sports_q2_correct_answer = Answer.create(answer_string: 'Baby Arizmendi')
sports_q2_correct_answer.question_id= sports_q2.id
sports_q2_correct_answer.is_correct= true
sports_q2_correct_answer.save

sports_q2_answer2 = Answer.create(answer_string: 'Teddy Baldock')
sports_q2_answer2.question_id= sports_q2.id
sports_q2_answer2.is_correct= false
sports_q2_answer2.save

sports_q2_answer3 = Answer.create(answer_string: 'Kid Laredo')
sports_q2_answer3.question_id= sports_q2.id
sports_q2_answer3.is_correct= false
sports_q2_answer3.save

sports_q2_answer4 = Answer.create(answer_string: 'Jack Dempsey')
sports_q2_answer4.question_id= sports_q2.id
sports_q2_answer4.is_correct= false
sports_q2_answer4.save


sports_q3 = Question.create(question_string: 'What yachting race was called the Hundred-Guinea Cup until a team from the U.S. won the race in 1851?')
sports_q3.category_id = Category.where(name: :sports).first.id
sports_q3.difficulty_level= 1
sports_q3.save

sports_q3_correct_answer = Answer.create(answer_string: "The America's Cup")
sports_q3_correct_answer.question_id= sports_q3.id
sports_q3_correct_answer.is_correct= true
sports_q3_correct_answer.save

sports_q3_answer2 = Answer.create(answer_string: "The Admiral's Cup")
sports_q3_answer2.question_id= sports_q3.id
sports_q3_answer2.is_correct= false
sports_q3_answer2.save

sports_q3_answer3 = Answer.create(answer_string: 'The Stanley Cup')
sports_q3_answer3.question_id= sports_q3.id
sports_q3_answer3.is_correct= false
sports_q3_answer3.save

sports_q3_answer4 = Answer.create(answer_string: 'The Gascoigne Cup')
sports_q3_answer4.question_id= sports_q3.id
sports_q3_answer4.is_correct= false
sports_q3_answer4.save


sports_q4 = Question.create(question_string: 'What golfer coined the term "caddy?"')
sports_q4.category_id = Category.where(name: :sports).first.id
sports_q4.difficulty_level= 1
sports_q4.save

sports_q4_correct_answer = Answer.create(answer_string: 'Mary Queen of Scots')
sports_q4_correct_answer.question_id= sports_q4.id
sports_q4_correct_answer.is_correct= true
sports_q4_correct_answer.save

sports_q4_answer2 = Answer.create(answer_string: 'Walter Hagen')
sports_q4_answer2.question_id= sports_q4.id
sports_q4_answer2.is_correct= false
sports_q4_answer2.save

sports_q4_answer3 = Answer.create(answer_string: 'Bobby Jones')
sports_q4_answer3.question_id= sports_q4.id
sports_q4_answer3.is_correct= false
sports_q4_answer3.save

sports_q4_answer4 = Answer.create(answer_string: 'Napolean')
sports_q4_answer4.question_id= sports_q4.id
sports_q4_answer4.is_correct= false
sports_q4_answer4.save


sports_q5 = Question.create(question_string: 'What was the first sport televised in the U.S.?')
sports_q5.category_id = Category.where(name: :sports).first.id
sports_q5.difficulty_level= 1
sports_q5.save

sports_q5_correct_answer = Answer.create(answer_string: 'Baseball')
sports_q5_correct_answer.question_id= sports_q5.id
sports_q5_correct_answer.is_correct= true
sports_q5_correct_answer.save

sports_q5_answer2 = Answer.create(answer_string: 'Tennis')
sports_q5_answer2.question_id= sports_q5.id
sports_q5_answer2.is_correct= false
sports_q5_answer2.save

sports_q5_answer3 = Answer.create(answer_string: 'Golf')
sports_q5_answer3.question_id= sports_q5.id
sports_q5_answer3.is_correct= false
sports_q5_answer3.save

sports_q5_answer4 = Answer.create(answer_string: 'Rugby')
sports_q5_answer4.question_id= sports_q5.id
sports_q5_answer4.is_correct= false
sports_q5_answer4.save



sports_q6 = Question.create(question_string: 'Who won the UEFA European Championship of 2000?')
sports_q6.category_id = Category.where(name: :art).first.id
sports_q6.difficulty_level= 1
sports_q6.save

sports_q6_correct_answer = Answer.create(answer_string: 'France')
sports_q6_correct_answer.question_id= sports_q6.id
sports_q6_correct_answer.is_correct= true
sports_q6_correct_answer.save

sports_q6_answer2 = Answer.create(answer_string: 'Italy')
sports_q6_answer2.question_id= sports_q6.id
sports_q6_answer2.is_correct= false
sports_q6_answer2.save

sports_q6_answer3 = Answer.create(answer_string: 'Portugal')
sports_q6_answer3.question_id= sports_q6.id
sports_q6_answer3.is_correct= false
sports_q6_answer3.save

sports_q6_answer4 = Answer.create(answer_string: 'Spain')
sports_q6_answer4.question_id= sports_q6.id
sports_q6_answer4.is_correct= false
sports_q6_answer4.save



sports_q7 = Question.create(question_string: 'What word means “zero” when calling the score in tennis?')
sports_q7.category_id = Category.where(name: :art).first.id
sports_q7.difficulty_level= 1
sports_q7.save

sports_q7_correct_answer = Answer.create(answer_string: 'Love')
sports_q7_correct_answer.question_id= sports_q7.id
sports_q7_correct_answer.is_correct= true
sports_q7_correct_answer.save

sports_q7_answer2 = Answer.create(answer_string: 'Dove')
sports_q7_answer2.question_id= sports_q7.id
sports_q7_answer2.is_correct= false
sports_q7_answer2.save

sports_q7_answer3 = Answer.create(answer_string: 'Nothing')
sports_q7_answer3.question_id= sports_q7.id
sports_q7_answer3.is_correct= false
sports_q7_answer3.save

sports_q7_answer4 = Answer.create(answer_string: 'Hug')
sports_q7_answer4.question_id= sports_q7.id
sports_q7_answer4.is_correct= false
sports_q7_answer4.save



sports_q8 = Question.create(question_string: 'Who was the youngest swimmer in the 2012 Olympics?')
sports_q8.category_id = Category.where(name: :art).first.id
sports_q8.difficulty_level= 1
sports_q8.save

sports_q8_correct_answer = Answer.create(answer_string: 'Missy Franklin')
sports_q8_correct_answer.question_id= sports_q8.id
sports_q8_correct_answer.is_correct= true
sports_q8_correct_answer.save

sports_q8_answer2 = Answer.create(answer_string: 'Nastia Lukin')
sports_q8_answer2.question_id= sports_q8.id
sports_q8_answer2.is_correct= false
sports_q8_answer2.save

sports_q8_answer3 = Answer.create(answer_string: 'Rebecca Soni')
sports_q8_answer3.question_id= sports_q8.id
sports_q8_answer3.is_correct= false
sports_q8_answer3.save

sports_q8_answer4 = Answer.create(answer_string: 'Michael Phelps')
sports_q8_answer4.question_id= sports_q8.id
sports_q8_answer4.is_correct= false
sports_q8_answer4.save



sports_q9 = Question.create(question_string: 'Which sport is played with a ball, racquet and safety goggles?')
sports_q9.category_id = Category.where(name: :art).first.id
sports_q9.difficulty_level= 1
sports_q9.save

sports_q9_correct_answer = Answer.create(answer_string: 'Racquetball')
sports_q9_correct_answer.question_id= sports_q9.id
sports_q9_correct_answer.is_correct= true
sports_q9_correct_answer.save

sports_q9_answer2 = Answer.create(answer_string: 'Table Tennis')
sports_q9_answer2.question_id= sports_q9.id
sports_q9_answer2.is_correct= false
sports_q9_answer2.save

sports_q9_answer3 = Answer.create(answer_string: 'Tennis')
sports_q9_answer3.question_id= sports_q9.id
sports_q9_answer3.is_correct= false
sports_q9_answer3.save

sports_q9_answer4 = Answer.create(answer_string: 'Badminton')
sports_q9_answer4.question_id= sports_q9.id
sports_q9_answer4.is_correct= false
sports_q9_answer4.save



sports_q10 = Question.create(question_string: "What sport doesn’t have a goalie position?")
sports_q10.category_id = Category.where(name: :art).first.id
sports_q10.difficulty_level= 1
sports_q10.save

sports_q10_correct_answer = Answer.create(answer_string: 'Basketball')
sports_q10_correct_answer.question_id= sports_q10.id
sports_q10_correct_answer.is_correct= true
sports_q10_correct_answer.save

sports_q10_answer2 = Answer.create(answer_string: 'Ice Hockey')
sports_q10_answer2.question_id= sports_q10.id
sports_q10_answer2.is_correct= false
sports_q10_answer2.save

sports_q10_answer3 = Answer.create(answer_string: 'Soccer')
sports_q10_answer3.question_id= sports_q10.id
sports_q10_answer3.is_correct= false
sports_q10_answer3.save

sports_q10_answer4 = Answer.create(answer_string: 'Lacrosse')
sports_q10_answer4.question_id= sports_q10.id
sports_q10_answer4.is_correct= false
sports_q10_answer4.save



sports_q11 = Question.create(question_string: 'What happens when a soccer player (not a goalkeeper) intentionally uses his or her hands in the box?')
sports_q11.category_id = Category.where(name: :art).first.id
sports_q11.difficulty_level= 1
sports_q11.save

sports_q11_correct_answer = Answer.create(answer_string: 'The other team gets a penalty kick')
sports_q11_correct_answer.question_id= sports_q11.id
sports_q11_correct_answer.is_correct= true
sports_q11_correct_answer.save

sports_q11_answer2 = Answer.create(answer_string: 'The other team gets to kick off')
sports_q11_answer2.question_id= sports_q11.id
sports_q11_answer2.is_correct= false
sports_q11_answer2.save

sports_q11_answer3 = Answer.create(answer_string: 'The other team gets to use their hands')
sports_q11_answer3.question_id= sports_q11.id
sports_q11_answer3.is_correct= false
sports_q11_answer3.save

sports_q11_answer4 = Answer.create(answer_string: 'The other team gets to add another player')
sports_q11_answer4.question_id= sports_q11.id
sports_q11_answer4.is_correct= false
sports_q11_answer4.save



sports_q12 = Question.create(question_string:'What happens when you sink an 8 ball before the rest of the ball?' )
sports_q12.category_id = Category.where(name: :art).first.id
sports_q12.difficulty_level= 1
sports_q12.save

sports_q12_correct_answer = Answer.create(answer_string: 'You automatically lose')
sports_q12_correct_answer.question_id= sports_q12.id
sports_q12_correct_answer.is_correct= true
sports_q12_correct_answer.save

sports_q12_answer2 = Answer.create(answer_string: 'You automatically win')
sports_q12_answer2.question_id= sports_q12.id
sports_q12_answer2.is_correct= false
sports_q12_answer2.save

sports_q12_answer3 = Answer.create(answer_string: 'It tells your fortune')
sports_q12_answer3.question_id= sports_q12.id
sports_q12_answer3.is_correct= false
sports_q12_answer3.save

sports_q12_answer4 = Answer.create(answer_string: 'It counts as a scratch')
sports_q12_answer4.question_id= sports_q12.id
sports_q12_answer4.is_correct= false
sports_q12_answer4.save
# </editor-fold>

Question.update_all(:active => true)