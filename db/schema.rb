# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150412185739) do

  create_table "answered_questions", force: true do |t|
    t.integer  "game_detail_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "correct_count",   default: 0
    t.integer  "incorrect_count", default: 0
  end

  add_index "answered_questions", ["category_id"], name: "index_answered_questions_on_category_id"

  create_table "answers", force: true do |t|
    t.integer  "question_id"
    t.boolean  "is_correct"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "answer_string"
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id"

  create_table "authentication_providers", force: true do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "authentication_providers", ["name"], name: "index_name_on_authentication_providers"

  create_table "average_caches", force: true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "avg",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "game_challenge_questions", force: true do |t|
    t.integer  "game_challenge_id"
    t.integer  "game_question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "game_challenges", force: true do |t|
    t.integer  "player_detail_id"
    t.integer  "opponent_detail_id"
    t.integer  "player_correct_count",     default: 0
    t.integer  "player_incorrect_count",   default: 0
    t.integer  "opponent_correct_count",   default: 0
    t.integer  "opponent_incorrect_count", default: 0
    t.integer  "winning_player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "opponent_game_piece_id"
    t.integer  "wager_game_piece_id"
    t.integer  "player_id"
    t.integer  "opponent_id"
    t.integer  "game_id"
  end

  create_table "game_detail_game_pieces", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "game_detail_id"
    t.integer  "game_piece_id"
  end

  create_table "game_detail_questions", force: true do |t|
    t.integer  "game_detail_id"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "question_answered"
  end

  create_table "game_details", force: true do |t|
    t.integer  "game_id"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "correct_answers",            default: 0
    t.integer  "questions_answered_in_turn", default: 0
    t.integer  "incorrect_answers",          default: 0
    t.boolean  "shown_crown_message",        default: false
  end

  add_index "game_details", ["game_id"], name: "index_game_details_on_game_id"

  create_table "game_pieces", force: true do |t|
    t.integer  "category_id"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "game_setups", force: true do |t|
    t.string   "opponent_selection_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "games", force: true do |t|
    t.integer  "player1_id"
    t.integer  "player2_id"
    t.integer  "player_turn"
    t.boolean  "finished"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "round",                 default: 1
    t.integer  "winning_player_id"
    t.integer  "resigned_by_player_id"
  end

  create_table "overall_averages", force: true do |t|
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "overall_avg",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "question_ratings", force: true do |t|
    t.integer  "question_id"
    t.integer  "user_id"
    t.integer  "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "question_ratings", ["question_id"], name: "index_question_ratings_on_question_id"
  add_index "question_ratings", ["user_id"], name: "index_question_ratings_on_user_id"

  create_table "questions", force: true do |t|
    t.integer  "category_id"
    t.integer  "difficulty_level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "question_string"
    t.boolean  "active"
    t.boolean  "reviewed",         default: false
  end

  add_index "questions", ["category_id"], name: "index_questions_on_category_id"

  create_table "rates", force: true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id"

  create_table "rating_caches", force: true do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type"

  create_table "user_authentications", force: true do |t|
    t.integer  "user_id"
    t.integer  "authentication_provider_id"
    t.string   "uid"
    t.string   "token"
    t.datetime "token_expires_at"
    t.text     "params"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "user_authentications", ["authentication_provider_id"], name: "index_user_authentications_on_authentication_provider_id"
  add_index "user_authentications", ["user_id"], name: "index_user_authentications_on_user_id"

  create_table "user_roles", force: true do |t|
    t.integer  "constant_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",                 default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "image"
    t.integer  "currency",                           default: 0
    t.integer  "role",                   limit: 255, default: 2
    t.string   "username"
    t.integer  "xp",                                 default: 0
    t.integer  "level",                              default: 1
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
