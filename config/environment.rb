# Load the Rails application.
require File.expand_path('../application', __FILE__)

ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                  587,
    domain:               'blog.com',
    user_name:            'KittredgeCapstone@gmail.com',
    password:             'HomewardBound',
    authentication:       'plain',
    enable_starttls_auto: true
}


# Initialize the Rails application.
Rails.application.initialize!
