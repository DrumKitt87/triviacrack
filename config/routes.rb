Rails.application.routes.draw do

  get 'admin/admin'

  get 'submit_question/create'

  resources :game_setups

  #admin routes
  get 'admin/admin/:page_number(/:keyword)', to:'admin#admin', as:'admin'
  get 'admin/edit_user/:id', to:'admin#edit_user', as:'admin_edit_user'
  post 'admin/update_user/:id', to:'admin#update_user', as:'admin_update_user'

  #gameplay routes
  get 'gameplay/spinner/:id', to: 'gameplay#spinner', as: 'gameplay_spinner'
  get 'gameplay/spin/:game_id', to: 'gameplay#spin', as: 'gameplay_spin'
  get 'gameplay/choose_category/:game_id', to: 'gameplay#choose_category', as:'gameplay_choose_category'
  get 'gameplay/challenge_select_pieces/:game_id', to: 'gameplay#challenge_select_pieces', as: 'challenge_select_pieces'
  get 'gameplay/challenge_setup', to: 'gameplay#challenge_setup', as: 'challenge_setup'
  get 'gameplay/challenge_next_question/:game_id/:challenge_id', to: 'gameplay#challenge_next_question', as: 'challenge_next_question'

  #question routes
  get 'question/question/:game_id/:category_id/:is_crown/:is_challenge(/:challenge_id)', to: 'question#question', as: 'question_with_params'
  get 'question/question', to: 'question#question', as: 'question'
  get 'question/answered_question/:game_id/:question_id/:answer_id/:is_crown/:is_challenge(/:challenge_id)(/:did_not_answer)', to: 'question#answered_question', as: 'answered_question'

  get 'powerup/fifty_fifty/:is_crown_or_challenge', to: 'powerup#fifty_fifty', as: 'power_up_fifty_fifty'
  get 'powerup/extend_time/:is_crown_or_challenge', to: 'powerup#extend_time', as: 'power_up_extend_time'
  get 'powerup/skip_question/:game_id/:question_id/:answer_id/:is_crown/:is_challenge(/:challenge_id)', to: 'powerup#skip_question', as: 'power_up_skip_question'

  #question_ratings routes
  get 'question_rating/show/:question_id', to: 'question_rating#show', as: 'show_question_rating'
  post 'question_rating/create', to: 'question_rating#create', as: 'question_ratings'

  #opponent search routes
  get 'search_for_opponents/select_opponent/:page_number(/:keyword)', to: 'search_for_opponents#select_opponent', as: 'select_opponent'
  get 'search_for_opponents/create_game', to: 'search_for_opponents#create_game', as: 'create_game_with_opponent'

  #home routes
  get 'home/index'
  post 'home/create_game_setup', to: 'home#create_game_setup', as: 'create_game_setup'
  get 'home/setup_new_game/:id', to: 'home#setup_new_game', as: 'setup_new_game'

  #profile routes
  get 'profile/view'

  #submit question routes
  get 'submit_question/new', to: 'submit_question#new', as: 'submit_question'
  post 'submit_question/create', to: 'submit_question#create', as: 'create_question'

  #review questions routes
  get 'question_review/review(/:page_number)', to: 'question_review#review', as: 'review_questions'
  get 'question_review/approve/:question_id/:page_number', to: 'question_review#approve', as: 'approve_question'
  get 'question_review/deny/:question_id/:page_number', to: 'question_review#deny', as: 'deny_question'

  #devise routes
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', registrations: 'registrations' }

  get 'welcome/index'

  root to: 'welcome#index'

end
